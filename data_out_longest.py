import numpy as np
import matplotlib.pyplot as plt
length_stack = 0
d = 8
w = 0.8
all_store_x = []
plt.figure(figsize=(100,9))
store0_x = [element + w +length_stack for element in range(1)]
length_stack = length_stack + 1 + w + 1

undefined = [0]
FD = [111]
Iss = [0]
OC = [350]
EX = [711]
WB = [0]
DataDepen = [62]
ibuffer = [0]
iss2iss = [64]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [32]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [789]
INTP = [1248]
ret = [32]
warp_schedule_latency = [1]
icnt = [450]
L1 = [96]
L2 = [2625]
control = []
plt.bar(store0_x,undefined,label="undefined",color='white')
plt.bar(store0_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store0_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store0_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store0_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store0_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store0_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store0_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store0_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store0_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store0_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store0_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store0_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store0_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store0_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store0_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store0_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store0_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store0_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store0_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.legend(loc="lower left",bbox_to_anchor=(1.02,0.3))


store1_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store1_x,undefined,label="undefined",color='white')
plt.bar(store1_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store1_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store1_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store1_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store1_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store1_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store1_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store1_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store1_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store1_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store1_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store1_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store1_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store1_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store1_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store1_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store1_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store1_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store1_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
store1_x = [element + w +length_stack for element in range(1)]
length_stack = length_stack + 1 + w + 1

undefined = [0]
FD = [96]
Iss = [0]
OC = [662]
EX = [366]
WB = [0]
DataDepen = [242]
ibuffer = [0]
iss2iss = [96]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [892]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [1255]
INTP = [4595]
ret = [2279]
warp_schedule_latency = [2]
icnt = [120]
L1 = [8910]
L2 = [14925]
control = []
plt.bar(store1_x,undefined,label="undefined",color='white')
plt.bar(store1_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store1_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store1_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store1_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store1_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store1_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store1_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store1_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store1_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store1_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store1_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store1_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store1_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store1_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store1_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store1_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store1_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store1_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store1_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')


store2_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store2_x,undefined,label="undefined",color='white')
plt.bar(store2_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store2_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store2_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store2_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store2_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store2_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store2_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store2_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store2_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store2_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store2_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store2_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store2_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store2_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store2_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store2_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store2_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store2_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store2_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
store2_x = [element + w +length_stack for element in range(1)]
length_stack = length_stack + 1 + w + 1

undefined = [0]
FD = [110]
Iss = [0]
OC = [348]
EX = [670]
WB = [0]
DataDepen = [60]
ibuffer = [0]
iss2iss = [64]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [32]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [762]
INTP = [1184]
ret = [6770]
warp_schedule_latency = [1]
icnt = [392]
L1 = [96]
L2 = [2464]
control = []
plt.bar(store2_x,undefined,label="undefined",color='white')
plt.bar(store2_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store2_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store2_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store2_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store2_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store2_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store2_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store2_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store2_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store2_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store2_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store2_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store2_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store2_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store2_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store2_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store2_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store2_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store2_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')


store3_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store3_x,undefined,label="undefined",color='white')
plt.bar(store3_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store3_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store3_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store3_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store3_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store3_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store3_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store3_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store3_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store3_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store3_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store3_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store3_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store3_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store3_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store3_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store3_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store3_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store3_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
store3_x = [element + w +length_stack for element in range(1)]
length_stack = length_stack + 1 + w + 1

undefined = [0]
FD = [96]
Iss = [0]
OC = [592]
EX = [348]
WB = [0]
DataDepen = [228]
ibuffer = [0]
iss2iss = [96]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [888]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [1214]
INTP = [4310]
ret = [2270]
warp_schedule_latency = [2]
icnt = [112]
L1 = [8484]
L2 = [13902]
control = []
plt.bar(store3_x,undefined,label="undefined",color='white')
plt.bar(store3_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store3_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store3_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store3_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store3_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store3_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store3_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store3_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store3_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store3_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store3_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store3_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store3_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store3_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store3_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store3_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store3_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store3_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store3_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')


store4_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store4_x,undefined,label="undefined",color='white')
plt.bar(store4_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store4_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store4_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store4_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store4_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store4_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store4_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store4_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store4_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store4_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store4_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store4_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store4_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store4_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store4_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store4_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store4_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store4_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store4_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
store4_x = [element + w +length_stack for element in range(1)]
length_stack = length_stack + 1 + w + 1

undefined = [0]
FD = [109]
Iss = [0]
OC = [346]
EX = [629]
WB = [0]
DataDepen = [58]
ibuffer = [0]
iss2iss = [64]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [32]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [735]
INTP = [1120]
ret = [6772]
warp_schedule_latency = [1]
icnt = [338]
L1 = [96]
L2 = [2301]
control = []
plt.bar(store4_x,undefined,label="undefined",color='white')
plt.bar(store4_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store4_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store4_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store4_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store4_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store4_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store4_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store4_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store4_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store4_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store4_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store4_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store4_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store4_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store4_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store4_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store4_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store4_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store4_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')


store5_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store5_x,undefined,label="undefined",color='white')
plt.bar(store5_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store5_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store5_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store5_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store5_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store5_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store5_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store5_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store5_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store5_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store5_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store5_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store5_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store5_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store5_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store5_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store5_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store5_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store5_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
store5_x = [element + w +length_stack for element in range(1)]
length_stack = length_stack + 1 + w + 1

undefined = [0]
FD = [96]
Iss = [0]
OC = [240]
EX = [330]
WB = [0]
DataDepen = [214]
ibuffer = [0]
iss2iss = [96]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [884]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [1173]
INTP = [3973]
ret = [2344]
warp_schedule_latency = [2]
icnt = [104]
L1 = [7020]
L2 = [12454]
control = []
plt.bar(store5_x,undefined,label="undefined",color='white')
plt.bar(store5_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store5_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store5_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store5_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store5_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store5_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store5_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store5_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store5_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store5_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store5_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store5_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store5_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store5_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store5_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store5_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store5_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store5_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store5_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')


store6_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store6_x,undefined,label="undefined",color='white')
plt.bar(store6_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store6_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store6_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store6_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store6_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store6_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store6_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store6_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store6_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store6_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store6_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store6_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store6_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store6_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store6_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store6_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store6_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store6_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store6_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
store6_x = [element + w +length_stack for element in range(1)]
length_stack = length_stack + 1 + w + 1

undefined = [0]
FD = [108]
Iss = [0]
OC = [344]
EX = [588]
WB = [0]
DataDepen = [56]
ibuffer = [0]
iss2iss = [64]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [32]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [708]
INTP = [1056]
ret = [6774]
warp_schedule_latency = [1]
icnt = [288]
L1 = [96]
L2 = [2136]
control = []
plt.bar(store6_x,undefined,label="undefined",color='white')
plt.bar(store6_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store6_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store6_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store6_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store6_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store6_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store6_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store6_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store6_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store6_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store6_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store6_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store6_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store6_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store6_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store6_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store6_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store6_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store6_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')


store7_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store7_x,undefined,label="undefined",color='white')
plt.bar(store7_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store7_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store7_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store7_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store7_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store7_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store7_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store7_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store7_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store7_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store7_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store7_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store7_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store7_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store7_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store7_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store7_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store7_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store7_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
store7_x = [element + w +length_stack for element in range(1)]
length_stack = length_stack + 1 + w + 1

undefined = [0]
FD = [96]
Iss = [0]
OC = [344]
EX = [312]
WB = [0]
DataDepen = [200]
ibuffer = [0]
iss2iss = [96]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [880]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [1132]
INTP = [3692]
ret = [2338]
warp_schedule_latency = [2]
icnt = [672]
L1 = [0]
L2 = [17328]
control = []
plt.bar(store7_x,undefined,label="undefined",color='white')
plt.bar(store7_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store7_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store7_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store7_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store7_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store7_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store7_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store7_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store7_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store7_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store7_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store7_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store7_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store7_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store7_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store7_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store7_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store7_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store7_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')


store8_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store8_x,undefined,label="undefined",color='white')
plt.bar(store8_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store8_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store8_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store8_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store8_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store8_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store8_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store8_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store8_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store8_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store8_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store8_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store8_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store8_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store8_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store8_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store8_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store8_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store8_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
store8_x = [element + w +length_stack for element in range(1)]
length_stack = length_stack + 1 + w + 1

undefined = [0]
FD = [107]
Iss = [0]
OC = [342]
EX = [547]
WB = [0]
DataDepen = [54]
ibuffer = [0]
iss2iss = [64]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [32]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [681]
INTP = [992]
ret = [6776]
warp_schedule_latency = [1]
icnt = [242]
L1 = [96]
L2 = [1969]
control = []
plt.bar(store8_x,undefined,label="undefined",color='white')
plt.bar(store8_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store8_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store8_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store8_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store8_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store8_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store8_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store8_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store8_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store8_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store8_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store8_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store8_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store8_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store8_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store8_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store8_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store8_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store8_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')


store9_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store9_x,undefined,label="undefined",color='white')
plt.bar(store9_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store9_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store9_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store9_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store9_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store9_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store9_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store9_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store9_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store9_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store9_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store9_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store9_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store9_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store9_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store9_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store9_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store9_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store9_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
store9_x = [element + w +length_stack for element in range(1)]
length_stack = length_stack + 1 + w + 1

undefined = [0]
FD = [96]
Iss = [0]
OC = [208]
EX = [294]
WB = [0]
DataDepen = [186]
ibuffer = [0]
iss2iss = [118]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [854]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [1091]
INTP = [3411]
ret = [2398]
warp_schedule_latency = [2]
icnt = [528]
L1 = [5588]
L2 = [9350]
control = []
plt.bar(store9_x,undefined,label="undefined",color='white')
plt.bar(store9_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store9_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store9_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store9_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store9_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store9_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store9_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store9_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store9_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store9_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store9_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store9_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store9_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store9_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store9_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store9_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store9_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store9_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store9_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')


store10_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store10_x,undefined,label="undefined",color='white')
plt.bar(store10_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store10_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store10_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store10_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store10_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store10_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store10_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store10_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store10_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store10_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store10_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store10_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store10_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store10_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store10_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store10_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store10_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store10_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store10_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
store10_x = [element + w +length_stack for element in range(1)]
length_stack = length_stack + 1 + w + 1

undefined = [0]
FD = [106]
Iss = [0]
OC = [340]
EX = [506]
WB = [0]
DataDepen = [52]
ibuffer = [0]
iss2iss = [64]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [32]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [654]
INTP = [928]
ret = [6778]
warp_schedule_latency = [1]
icnt = [200]
L1 = [96]
L2 = [1800]
control = []
plt.bar(store10_x,undefined,label="undefined",color='white')
plt.bar(store10_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store10_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store10_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store10_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store10_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store10_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store10_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store10_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store10_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store10_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store10_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store10_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store10_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store10_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store10_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store10_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store10_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store10_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store10_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')


store11_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store11_x,undefined,label="undefined",color='white')
plt.bar(store11_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store11_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store11_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store11_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store11_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store11_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store11_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store11_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store11_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store11_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store11_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store11_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store11_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store11_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store11_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store11_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store11_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store11_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store11_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
store11_x = [element + w +length_stack for element in range(1)]
length_stack = length_stack + 1 + w + 1

undefined = [0]
FD = [96]
Iss = [0]
OC = [192]
EX = [276]
WB = [0]
DataDepen = [172]
ibuffer = [0]
iss2iss = [116]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [852]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [1050]
INTP = [3130]
ret = [2394]
warp_schedule_latency = [2]
icnt = [440]
L1 = [4980]
L2 = [8670]
control = []
plt.bar(store11_x,undefined,label="undefined",color='white')
plt.bar(store11_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store11_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store11_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store11_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store11_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store11_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store11_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store11_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store11_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store11_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store11_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store11_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store11_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store11_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store11_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store11_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store11_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store11_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store11_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')


store12_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store12_x,undefined,label="undefined",color='white')
plt.bar(store12_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store12_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store12_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store12_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store12_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store12_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store12_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store12_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store12_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store12_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store12_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store12_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store12_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store12_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store12_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store12_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store12_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store12_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store12_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
store12_x = [element + w +length_stack for element in range(1)]
length_stack = length_stack + 1 + w + 1

undefined = [0]
FD = [105]
Iss = [0]
OC = [338]
EX = [465]
WB = [0]
DataDepen = [50]
ibuffer = [0]
iss2iss = [64]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [32]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [627]
INTP = [864]
ret = [6780]
warp_schedule_latency = [1]
icnt = [162]
L1 = [96]
L2 = [1629]
control = []
plt.bar(store12_x,undefined,label="undefined",color='white')
plt.bar(store12_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store12_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store12_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store12_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store12_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store12_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store12_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store12_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store12_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store12_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store12_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store12_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store12_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store12_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store12_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store12_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store12_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store12_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store12_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')


store13_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store13_x,undefined,label="undefined",color='white')
plt.bar(store13_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store13_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store13_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store13_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store13_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store13_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store13_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store13_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store13_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store13_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store13_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store13_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store13_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store13_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store13_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store13_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store13_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store13_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store13_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
store13_x = [element + w +length_stack for element in range(1)]
length_stack = length_stack + 1 + w + 1

undefined = [0]
FD = [96]
Iss = [0]
OC = [86]
EX = [258]
WB = [0]
DataDepen = [158]
ibuffer = [0]
iss2iss = [114]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [850]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [1009]
INTP = [2849]
ret = [2492]
warp_schedule_latency = [2]
icnt = [684]
L1 = [0]
L2 = [10494]
control = []
plt.bar(store13_x,undefined,label="undefined",color='white')
plt.bar(store13_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store13_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store13_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store13_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store13_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store13_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store13_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store13_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store13_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store13_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store13_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store13_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store13_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store13_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store13_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store13_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store13_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store13_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store13_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')


store14_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store14_x,undefined,label="undefined",color='white')
plt.bar(store14_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store14_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store14_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store14_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store14_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store14_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store14_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store14_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store14_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store14_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store14_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store14_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store14_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store14_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store14_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store14_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store14_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store14_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store14_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
store14_x = [element + w +length_stack for element in range(1)]
length_stack = length_stack + 1 + w + 1

undefined = [0]
FD = [104]
Iss = [0]
OC = [336]
EX = [424]
WB = [0]
DataDepen = [48]
ibuffer = [0]
iss2iss = [64]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [32]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [600]
INTP = [800]
ret = [6782]
warp_schedule_latency = [1]
icnt = [128]
L1 = [96]
L2 = [1456]
control = []
plt.bar(store14_x,undefined,label="undefined",color='white')
plt.bar(store14_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store14_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store14_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store14_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store14_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store14_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store14_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store14_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store14_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store14_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store14_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store14_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store14_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store14_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store14_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store14_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store14_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store14_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store14_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')


store15_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store15_x,undefined,label="undefined",color='white')
plt.bar(store15_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store15_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store15_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store15_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store15_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store15_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store15_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store15_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store15_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store15_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store15_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store15_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store15_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store15_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store15_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store15_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store15_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store15_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store15_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
store15_x = [element + w +length_stack for element in range(1)]
length_stack = length_stack + 1 + w + 1

undefined = [0]
FD = [96]
Iss = [0]
OC = [128]
EX = [240]
WB = [0]
DataDepen = [144]
ibuffer = [0]
iss2iss = [112]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [872]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [968]
INTP = [2568]
ret = [2437]
warp_schedule_latency = [2]
icnt = [800]
L1 = [0]
L2 = [9824]
control = []
plt.bar(store15_x,undefined,label="undefined",color='white')
plt.bar(store15_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store15_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store15_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store15_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store15_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store15_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store15_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store15_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store15_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store15_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store15_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store15_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store15_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store15_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store15_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store15_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store15_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store15_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store15_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')


store16_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store16_x,undefined,label="undefined",color='white')
plt.bar(store16_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store16_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store16_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store16_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store16_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store16_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store16_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store16_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store16_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store16_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store16_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store16_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store16_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store16_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store16_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store16_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store16_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store16_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store16_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
store16_x = [element + w +length_stack for element in range(1)]
length_stack = length_stack + 1 + w + 1

undefined = [0]
FD = [103]
Iss = [0]
OC = [334]
EX = [383]
WB = [0]
DataDepen = [46]
ibuffer = [0]
iss2iss = [64]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [32]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [573]
INTP = [736]
ret = [6784]
warp_schedule_latency = [1]
icnt = [98]
L1 = [96]
L2 = [1281]
control = []
plt.bar(store16_x,undefined,label="undefined",color='white')
plt.bar(store16_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store16_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store16_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store16_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store16_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store16_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store16_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store16_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store16_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store16_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store16_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store16_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store16_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store16_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store16_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store16_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store16_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store16_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store16_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')


store17_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store17_x,undefined,label="undefined",color='white')
plt.bar(store17_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store17_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store17_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store17_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store17_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store17_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store17_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store17_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store17_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store17_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store17_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store17_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store17_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store17_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store17_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store17_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store17_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store17_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store17_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
store17_x = [element + w +length_stack for element in range(1)]
length_stack = length_stack + 1 + w + 1

undefined = [0]
FD = [96]
Iss = [0]
OC = [74]
EX = [222]
WB = [0]
DataDepen = [130]
ibuffer = [0]
iss2iss = [110]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [846]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [927]
INTP = [2287]
ret = [2658]
warp_schedule_latency = [2]
icnt = [406]
L1 = [154]
L2 = [6993]
control = []
plt.bar(store17_x,undefined,label="undefined",color='white')
plt.bar(store17_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store17_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store17_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store17_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store17_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store17_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store17_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store17_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store17_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store17_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store17_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store17_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store17_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store17_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store17_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store17_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store17_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store17_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store17_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')


store18_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store18_x,undefined,label="undefined",color='white')
plt.bar(store18_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store18_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store18_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store18_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store18_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store18_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store18_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store18_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store18_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store18_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store18_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store18_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store18_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store18_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store18_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store18_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store18_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store18_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store18_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
store18_x = [element + w +length_stack for element in range(1)]
length_stack = length_stack + 1 + w + 1

undefined = [0]
FD = [102]
Iss = [0]
OC = [332]
EX = [342]
WB = [0]
DataDepen = [44]
ibuffer = [0]
iss2iss = [64]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [32]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [546]
INTP = [672]
ret = [6786]
warp_schedule_latency = [1]
icnt = [72]
L1 = [96]
L2 = [1104]
control = []
plt.bar(store18_x,undefined,label="undefined",color='white')
plt.bar(store18_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store18_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store18_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store18_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store18_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store18_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store18_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store18_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store18_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store18_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store18_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store18_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store18_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store18_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store18_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store18_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store18_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store18_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store18_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')


store19_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store19_x,undefined,label="undefined",color='white')
plt.bar(store19_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store19_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store19_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store19_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store19_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store19_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store19_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store19_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store19_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store19_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store19_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store19_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store19_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store19_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store19_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store19_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store19_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store19_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store19_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
store19_x = [element + w +length_stack for element in range(1)]
length_stack = length_stack + 1 + w + 1

undefined = [0]
FD = [96]
Iss = [0]
OC = [68]
EX = [204]
WB = [0]
DataDepen = [116]
ibuffer = [0]
iss2iss = [108]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [844]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [886]
INTP = [2006]
ret = [2666]
warp_schedule_latency = [2]
icnt = [300]
L1 = [132]
L2 = [5946]
control = []
plt.bar(store19_x,undefined,label="undefined",color='white')
plt.bar(store19_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store19_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store19_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store19_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store19_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store19_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store19_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store19_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store19_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store19_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store19_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store19_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store19_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store19_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store19_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store19_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store19_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store19_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store19_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')


store20_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store20_x,undefined,label="undefined",color='white')
plt.bar(store20_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store20_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store20_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store20_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store20_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store20_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store20_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store20_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store20_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store20_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store20_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store20_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store20_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store20_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store20_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store20_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store20_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store20_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store20_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
store20_x = [element + w +length_stack for element in range(1)]
length_stack = length_stack + 1 + w + 1

undefined = [0]
FD = [101]
Iss = [0]
OC = [330]
EX = [301]
WB = [0]
DataDepen = [42]
ibuffer = [0]
iss2iss = [64]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [32]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [519]
INTP = [608]
ret = [6788]
warp_schedule_latency = [1]
icnt = [50]
L1 = [96]
L2 = [925]
control = []
plt.bar(store20_x,undefined,label="undefined",color='white')
plt.bar(store20_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store20_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store20_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store20_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store20_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store20_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store20_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store20_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store20_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store20_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store20_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store20_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store20_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store20_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store20_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store20_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store20_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store20_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store20_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')


store21_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store21_x,undefined,label="undefined",color='white')
plt.bar(store21_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store21_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store21_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store21_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store21_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store21_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store21_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store21_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store21_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store21_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store21_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store21_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store21_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store21_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store21_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store21_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store21_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store21_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store21_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
store21_x = [element + w +length_stack for element in range(1)]
length_stack = length_stack + 1 + w + 1

undefined = [0]
FD = [96]
Iss = [0]
OC = [62]
EX = [186]
WB = [0]
DataDepen = [102]
ibuffer = [0]
iss2iss = [106]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [842]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [845]
INTP = [1725]
ret = [2683]
warp_schedule_latency = [2]
icnt = [210]
L1 = [110]
L2 = [4825]
control = []
plt.bar(store21_x,undefined,label="undefined",color='white')
plt.bar(store21_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store21_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store21_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store21_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store21_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store21_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store21_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store21_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store21_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store21_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store21_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store21_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store21_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store21_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store21_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store21_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store21_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store21_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store21_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')


store22_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store22_x,undefined,label="undefined",color='white')
plt.bar(store22_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store22_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store22_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store22_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store22_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store22_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store22_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store22_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store22_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store22_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store22_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store22_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store22_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store22_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store22_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store22_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store22_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store22_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store22_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
store22_x = [element + w +length_stack for element in range(1)]
length_stack = length_stack + 1 + w + 1

undefined = [0]
FD = [100]
Iss = [0]
OC = [328]
EX = [260]
WB = [0]
DataDepen = [40]
ibuffer = [0]
iss2iss = [64]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [32]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [492]
INTP = [544]
ret = [6790]
warp_schedule_latency = [1]
icnt = [32]
L1 = [96]
L2 = [744]
control = []
plt.bar(store22_x,undefined,label="undefined",color='white')
plt.bar(store22_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store22_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store22_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store22_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store22_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store22_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store22_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store22_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store22_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store22_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store22_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store22_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store22_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store22_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store22_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store22_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store22_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store22_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store22_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')


store23_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store23_x,undefined,label="undefined",color='white')
plt.bar(store23_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store23_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store23_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store23_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store23_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store23_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store23_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store23_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store23_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store23_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store23_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store23_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store23_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store23_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store23_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store23_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store23_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store23_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store23_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
store23_x = [element + w +length_stack for element in range(1)]
length_stack = length_stack + 1 + w + 1

undefined = [0]
FD = [96]
Iss = [0]
OC = [56]
EX = [168]
WB = [0]
DataDepen = [88]
ibuffer = [0]
iss2iss = [104]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [840]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [804]
INTP = [1444]
ret = [2689]
warp_schedule_latency = [2]
icnt = [136]
L1 = [88]
L2 = [3844]
control = []
plt.bar(store23_x,undefined,label="undefined",color='white')
plt.bar(store23_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store23_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store23_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store23_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store23_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store23_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store23_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store23_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store23_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store23_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store23_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store23_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store23_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store23_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store23_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store23_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store23_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store23_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store23_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')


store24_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store24_x,undefined,label="undefined",color='white')
plt.bar(store24_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store24_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store24_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store24_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store24_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store24_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store24_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store24_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store24_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store24_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store24_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store24_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store24_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store24_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store24_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store24_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store24_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store24_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store24_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
store24_x = [element + w +length_stack for element in range(1)]
length_stack = length_stack + 1 + w + 1

undefined = [0]
FD = [99]
Iss = [0]
OC = [326]
EX = [219]
WB = [0]
DataDepen = [38]
ibuffer = [0]
iss2iss = [64]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [32]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [465]
INTP = [480]
ret = [6791]
warp_schedule_latency = [1]
icnt = [18]
L1 = [96]
L2 = [561]
control = []
plt.bar(store24_x,undefined,label="undefined",color='white')
plt.bar(store24_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store24_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store24_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store24_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store24_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store24_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store24_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store24_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store24_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store24_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store24_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store24_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store24_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store24_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store24_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store24_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store24_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store24_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store24_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')


store25_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store25_x,undefined,label="undefined",color='white')
plt.bar(store25_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store25_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store25_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store25_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store25_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store25_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store25_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store25_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store25_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store25_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store25_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store25_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store25_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store25_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store25_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store25_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store25_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store25_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store25_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
store25_x = [element + w +length_stack for element in range(1)]
length_stack = length_stack + 1 + w + 1

undefined = [0]
FD = [96]
Iss = [0]
OC = [50]
EX = [150]
WB = [0]
DataDepen = [74]
ibuffer = [0]
iss2iss = [102]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [838]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [763]
INTP = [1163]
ret = [2700]
warp_schedule_latency = [2]
icnt = [78]
L1 = [66]
L2 = [2841]
control = []
plt.bar(store25_x,undefined,label="undefined",color='white')
plt.bar(store25_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store25_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store25_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store25_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store25_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store25_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store25_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store25_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store25_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store25_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store25_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store25_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store25_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store25_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store25_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store25_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store25_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store25_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store25_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')


store26_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store26_x,undefined,label="undefined",color='white')
plt.bar(store26_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store26_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store26_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store26_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store26_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store26_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store26_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store26_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store26_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store26_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store26_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store26_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store26_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store26_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store26_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store26_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store26_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store26_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store26_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
store26_x = [element + w +length_stack for element in range(1)]
length_stack = length_stack + 1 + w + 1

undefined = [0]
FD = [98]
Iss = [0]
OC = [324]
EX = [178]
WB = [0]
DataDepen = [36]
ibuffer = [0]
iss2iss = [64]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [32]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [438]
INTP = [416]
ret = [6792]
warp_schedule_latency = [1]
icnt = [8]
L1 = [96]
L2 = [376]
control = []
plt.bar(store26_x,undefined,label="undefined",color='white')
plt.bar(store26_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store26_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store26_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store26_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store26_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store26_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store26_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store26_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store26_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store26_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store26_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store26_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store26_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store26_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store26_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store26_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store26_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store26_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store26_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')


store27_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store27_x,undefined,label="undefined",color='white')
plt.bar(store27_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store27_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store27_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store27_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store27_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store27_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store27_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store27_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store27_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store27_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store27_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store27_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store27_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store27_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store27_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store27_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store27_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store27_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store27_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
store27_x = [element + w +length_stack for element in range(1)]
length_stack = length_stack + 1 + w + 1

undefined = [0]
FD = [96]
Iss = [0]
OC = [44]
EX = [132]
WB = [0]
DataDepen = [60]
ibuffer = [0]
iss2iss = [100]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [836]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [722]
INTP = [882]
ret = [2704]
warp_schedule_latency = [2]
icnt = [36]
L1 = [44]
L2 = [1894]
control = []
plt.bar(store27_x,undefined,label="undefined",color='white')
plt.bar(store27_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store27_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store27_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store27_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store27_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store27_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store27_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store27_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store27_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store27_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store27_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store27_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store27_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store27_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store27_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store27_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store27_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store27_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store27_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')


store28_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store28_x,undefined,label="undefined",color='white')
plt.bar(store28_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store28_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store28_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store28_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store28_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store28_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store28_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store28_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store28_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store28_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store28_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store28_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store28_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store28_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store28_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store28_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store28_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store28_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store28_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
store28_x = [element + w +length_stack for element in range(1)]
length_stack = length_stack + 1 + w + 1

undefined = [0]
FD = [97]
Iss = [0]
OC = [322]
EX = [137]
WB = [0]
DataDepen = [34]
ibuffer = [0]
iss2iss = [64]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [32]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [411]
INTP = [352]
ret = [6793]
warp_schedule_latency = [1]
icnt = [2]
L1 = [96]
L2 = [189]
control = []
plt.bar(store28_x,undefined,label="undefined",color='white')
plt.bar(store28_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store28_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store28_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store28_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store28_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store28_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store28_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store28_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store28_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store28_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store28_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store28_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store28_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store28_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store28_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store28_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store28_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store28_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store28_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')


store29_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store29_x,undefined,label="undefined",color='white')
plt.bar(store29_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store29_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store29_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store29_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store29_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store29_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store29_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store29_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store29_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store29_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store29_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store29_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store29_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store29_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store29_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store29_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store29_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store29_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store29_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
store29_x = [element + w +length_stack for element in range(1)]
length_stack = length_stack + 1 + w + 1

undefined = [0]
FD = [96]
Iss = [0]
OC = [38]
EX = [114]
WB = [0]
DataDepen = [46]
ibuffer = [0]
iss2iss = [98]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [834]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [681]
INTP = [601]
ret = [2709]
warp_schedule_latency = [2]
icnt = [10]
L1 = [22]
L2 = [945]
control = []
plt.bar(store29_x,undefined,label="undefined",color='white')
plt.bar(store29_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store29_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store29_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store29_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store29_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store29_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store29_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store29_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store29_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store29_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store29_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store29_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store29_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store29_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store29_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store29_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store29_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store29_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store29_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')


store30_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store30_x,undefined,label="undefined",color='white')
plt.bar(store30_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store30_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store30_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store30_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store30_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store30_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store30_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store30_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store30_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store30_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store30_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store30_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store30_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store30_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store30_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store30_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store30_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store30_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store30_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
#plt.show()
plt.savefig('out_longest.png')
