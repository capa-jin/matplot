
output = open("./data_out", "w")
input = open("./data_in","r")


priv_start_cycle = 0
pc = 0
warp_id = 0
sid = '0'
inst_num = 0

warps = []
undefined = []
FD = []
Iss = []
OC = []
EX = []
WB = []
DataDepen = []
ibuffer = []
iss2iss = []
MEM = []
Fetch_wait = []
Issue_wait = []
OC_wait = []
EX_wait = []
WB_wait = []
barrier = []

g_warps = []
g_undefined = []
g_FD = []
g_Iss = []
g_OC = []
g_EX = []
g_WB = []
g_DataDepen = []
g_ibuffer = []
g_iss2iss = []
g_MEM = []
g_Fetch_wait = []
g_Issue_wait = []
g_OC_wait = []
g_EX_wait = []
g_WB_wait = []
g_barrier = []

while True:
     ##READ line
    line = input.readline()
    if not line: break
    latencys = line.split(' ')
    #print(latencys)
    

    if latencys[0]!= sid:#warp up shader data
        
        output.write("\n")
        output.write("sid = " + sid +"\n")
        output.write("undefined = ["+ ','.join(undefined) +"]\n")
        output.write("FD = ["+ ','.join(FD) +"]\n")
        output.write("Iss = ["+ ','.join(Iss) +"]\n")
        output.write("OC = ["+ ','.join(OC) +"]\n")
        output.write("EX = ["+ ','.join(EX) +"]\n")
        output.write("WB = ["+ ','.join(WB) +"]\n")
        output.write("DataDepen = ["+ ','.join(DataDepen) +"]\n")
        output.write("ibuffer = ["+ ','.join(ibuffer) +"]\n")
        output.write("iss2iss = ["+ ','.join(iss2iss) +"]\n")
        output.write("MEM = ["+ ','.join(MEM) +"]\n")
        output.write("Fetch_wait = ["+ ','.join(Fetch_wait) +"]\n")
        output.write("Issue_wait = ["+ ','.join(Issue_wait) +"]\n")
        output.write("OC_wait = ["+ ','.join(OC_wait) +"]\n")
        output.write("EX_wait = ["+ ','.join(EX_wait) +"]\n")
        output.write("WB_wait = ["+ ','.join(WB_wait) +"]\n")
        output.write("barrier = ["+ ','.join(barrier) +"]\n")
        
        warp_data = ""
        output.write("warp = [")
        for warp_num in warps:
            warp_data += "\"warp " + warp_num + "\""+','
        warp_data = warp_data[:-1]
        warp_data += "]"

        output.write(warp_data)

        
        del undefined[:]
        del FD[:]
        del Iss[:]
        del OC[:]
        del EX[:]
        del WB[:]
        del DataDepen[:]
        del ibuffer[:]
        del iss2iss[:]
        del MEM[:]
        del Fetch_wait[:]
        del Issue_wait[:]
        del OC_wait[:]
        del EX_wait[:]
        del WB_wait[:]
        del barrier[:]
        del warps[:]
        sid = latencys[0]

    else:
        warps.append(latencys[1])
        undefined.append(latencys[3])
        FD.append(latencys[4])
        Iss.append(latencys[5])
        OC.append(latencys[6])
        EX.append(latencys[7])
        WB.append(latencys[8])
        DataDepen.append(latencys[9])
        ibuffer.append(latencys[10])
        iss2iss.append(latencys[11])
        MEM.append(latencys[12])
        Fetch_wait.append(latencys[13])
        Issue_wait.append(latencys[14])
        OC_wait.append(latencys[15])
        EX_wait.append(latencys[16])
        WB_wait.append(latencys[17])
        barrier.append(latencys[18])
        
        g_warps.append(sid+"-"+latencys[1])
        g_undefined.append(latencys[3])
        g_FD.append(latencys[4])
        g_Iss.append(latencys[5])
        g_OC.append(latencys[6])
        g_EX.append(latencys[7])
        g_WB.append(latencys[8])
        g_DataDepen.append(latencys[9])
        g_ibuffer.append(latencys[10])
        g_iss2iss.append(latencys[11])
        g_MEM.append(latencys[12])
        g_Fetch_wait.append(latencys[13])
        g_Issue_wait.append(latencys[14])
        g_OC_wait.append(latencys[15])
        g_EX_wait.append(latencys[16])
        g_WB_wait.append(latencys[17])
        g_barrier.append(latencys[18])


output.write("\n")
output.write("sid = " + sid +"\n")
output.write("undefined = ["+ ','.join(g_undefined) +"]\n")
output.write("FD = ["+ ','.join(g_FD) +"]\n")
output.write("Iss = ["+ ','.join(g_Iss) +"]\n")
output.write("OC = ["+ ','.join(g_OC) +"]\n")
output.write("EX = ["+ ','.join(g_EX) +"]\n")
output.write("WB = ["+ ','.join(g_WB) +"]\n")
output.write("DataDepen = ["+ ','.join(g_DataDepen) +"]\n")
output.write("ibuffer = ["+ ','.join(g_ibuffer) +"]\n")
output.write("iss2iss = ["+ ','.join(g_iss2iss) +"]\n")
output.write("MEM = ["+ ','.join(g_MEM) +"]\n")
output.write("Fetch_wait = ["+ ','.join(g_Fetch_wait) +"]\n")
output.write("Issue_wait = ["+ ','.join(g_Issue_wait) +"]\n")
output.write("OC_wait = ["+ ','.join(g_OC_wait) +"]\n")
output.write("EX_wait = ["+ ','.join(g_EX_wait) +"]\n")
output.write("WB_wait = ["+ ','.join(g_WB_wait) +"]\n")
output.write("barrier = ["+ ','.join(g_barrier) +"]\n")

g_warp_data = ""
output.write("warp = [")
temp_num = 0
for warp_num in g_warps:
    g_warp_data += "\"" + str(temp_num) + "\""+','
    temp_num += 1
g_warp_data = g_warp_data[:-1]
g_warp_data += "]"

output.write(g_warp_data)

output.close()

input.close()

     