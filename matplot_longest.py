

from numpy import log2


def make_stack(sid):
    output_str = ""
    output_str = "plt.bar(store"+ str(sid) +"_x,undefined,label=\"undefined\",color='white')\n"
    output_str = output_str + "plt.bar(store"+ str(sid) +"_x,warp_schedule_latency,bottom=np.array(undefined),label=\"warp_schedule_latency\",color='cyan')\n"
    output_str = output_str + "plt.bar(store"+ str(sid) +"_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label=\"ret\",color='black')\n"
    output_str = output_str + "plt.bar(store"+ str(sid) +"_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label=\"FD\",color='brown')\n"
    output_str = output_str + "plt.bar(store"+ str(sid) +"_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label=\"OC\",color='green')\n"
    output_str = output_str + "plt.bar(store"+ str(sid) +"_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label=\"EX\",color='linen')\n"
    output_str = output_str + "plt.bar(store"+ str(sid) +"_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label=\"WB\",color='purple')\n"
    output_str = output_str + "plt.bar(store"+ str(sid) +"_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label=\"DataDepen\",color='gray')\n"
    output_str = output_str + "plt.bar(store"+ str(sid) +"_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label=\"ibuffer\",color='olive')\n"
    output_str = output_str + "plt.bar(store"+ str(sid) +"_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label=\"iss2iss\",color='cyan')\n"
    output_str = output_str + "plt.bar(store"+ str(sid) +"_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label=\"dram_lat\",color='yellow')\n"
    output_str = output_str + "plt.bar(store"+ str(sid) +"_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label=\"Fetch_wait\",color='black')\n"
    output_str = output_str + "plt.bar(store"+ str(sid) +"_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label=\"Issue_wait\",color='navy')\n"
    output_str = output_str + "plt.bar(store"+ str(sid) +"_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label=\"barrier\",color='pink')\n"
    output_str = output_str + "plt.bar(store"+ str(sid) +"_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label=\"dram_access\",color='orange')\n"
    output_str = output_str + "plt.bar(store"+ str(sid) +"_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label=\"ALU\",color='purple')\n"
    output_str = output_str + "plt.bar(store"+ str(sid) +"_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label=\"INTP\",color='greenyellow')\n"
    output_str = output_str + "plt.bar(store"+ str(sid) +"_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label=\"icnt\",color='red')\n"
    output_str = output_str + "plt.bar(store"+ str(sid) +"_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label=\"L1\",color='royalblue')\n"
    output_str = output_str + "plt.bar(store"+ str(sid) +"_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label=\"L2\",color='lightcoral')\n"
    #output_str = output_str + "plt.bar(store"+ str(sid) +"_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label=\"control\",color='gray')\n"
    return output_str


output = open("./data_out_longest.py", "w")
input = open("./data_in","r")
output.write("import numpy as np\nimport matplotlib.pyplot as plt\nlength_stack = 0\n")
output.write("d = 8\nw = 0.8\nall_store_x = []\n")

output.write("plt.figure(figsize=(100,9))\n")


priv_start_cycle = 0
pc = 0
warp_id = 0
sid = 0
inst_num = 0

warps = []
undefined = []
FD = []
Iss = []
OC = []
EX = []
WB = []
DataDepen = []
ibuffer = []
iss2iss = []
MEM = []
Fetch_wait = []
Issue_wait = []
OC_wait = []
EX_wait = []
WB_wait = []
barrier = []
global_access = []
ALU = []
INTP = []
ret = []
warp_schedule_latency = []
icnt = []
L1 = []
L2 = []
control = []


kernel = [None] * 100
longest = [None] * 100
kernel[0] = [None]*300
longest[0] = [None]*300

kernel[1] = [None]*300
longest[1] = [None]*300
# if error with this arr, expend kernel arr legnth

g_warps = []
g_undefined = []
g_FD = []
g_Iss = []
g_OC = []
g_EX = []
g_WB = []
g_DataDepen = []
g_ibuffer = []
g_iss2iss = []
g_MEM = []
g_Fetch_wait = []
g_Issue_wait = []
g_OC_wait = []
g_EX_wait = []
g_WB_wait = []
g_barrier = []

priv_ker_id = 0

    
while True:
     ##READ line
    line = input.readline()
    if not line: break
    latencys = line.split(' ')
    #print(latencys)
    if priv_ker_id < int(latencys[0]) :
        priv_ker_id = int(latencys[0])
        kernel[priv_ker_id] = [None]*300
        longest[priv_ker_id] = [None]*300 #TB num
        kernel[priv_ker_id][int(latencys[2])] = [line[(line.find(":")+1):]]
    
    else:
        if kernel[int(latencys[0])][int(latencys[2])]==None:
            kernel[int(latencys[0])][int(latencys[2])] = [line[(line.find(":")+1):]]
        else:
            kernel[int(latencys[0])][int(latencys[2])].append(line[(line.find(":")+1):])


#print(kernel)

box_printed = 0
kernel_id = -1



box_printed = 0
kernel_id = -1
for ele in kernel:
    kernel_id = kernel_id + 1
    sid = -1
    if ele == None: continue

    for warp in ele:
        sid = sid + 1
        max_tb = 0
        if warp == None: continue
        for line in warp:
            if not line: break
            latencys = line.split(' ')
            temp = 0
            for stage in latencys:
                if(stage == "" or stage == '\n'): 
                    continue
                temp = temp + int(stage)
            if(temp > max_tb):
                #print(kernel_id)
                #print(longest[kernel_id][TB_id])
                #print(latencys[1:25])
                longest[kernel_id][sid] = latencys[1:25]
                max_tb = temp

#print(longest[1])
print(longest)
box_printed = 0
sid = 0
for ele in longest:
    if ele == None or ele == [None]*300 : continue
    for warp in ele:
        if warp == None: continue
        print(warp)
        undefined.append(warp[0])
        FD.append(warp[1])
        Iss.append(warp[2])
        OC.append(warp[3])
        EX.append(warp[4])
        WB.append(warp[5])
        DataDepen.append(warp[6])
        ibuffer.append(warp[7])
        iss2iss.append(warp[8])
        MEM.append(warp[9])
        Fetch_wait.append(warp[10])
        Issue_wait.append(warp[11])
        OC_wait.append(warp[12])
        EX_wait.append(warp[13])
        WB_wait.append(warp[14])
        barrier.append(warp[15])
        global_access.append(warp[16])
        ALU.append(warp[17])
        INTP.append(warp[18])
        ret.append(warp[19])
        warp_schedule_latency.append(warp[20])
        icnt.append(warp[21])
        L1.append(warp[22])
        L2.append(warp[23])
        #control.append(warp[24])
        
        
    output.write("store" + str(sid) +"_x = [element + w +length_stack for element in range("+str(len(undefined))+")]\n")
    output.write("length_stack = length_stack + "+str(len(undefined))+" + w + 1\n")
    """if sid == 0:
        output.write("all_store_x = store1_x\n")
        
    else:
        output.write("for ele in store"+str(sid)+"_x:\n    all_store_x.append(ele)\n")
    """    

    output.write("\n")
    output.write("undefined = ["+ ','.join(undefined) +"]\n")
    output.write("FD = ["+ ','.join(FD) +"]\n")
    output.write("Iss = ["+ ','.join(Iss) +"]\n")
    output.write("OC = ["+ ','.join(OC) +"]\n")
    output.write("EX = ["+ ','.join(EX) +"]\n")
    output.write("WB = ["+ ','.join(WB) +"]\n")
    output.write("DataDepen = ["+ ','.join(DataDepen) +"]\n")
    output.write("ibuffer = ["+ ','.join(ibuffer) +"]\n")
    output.write("iss2iss = ["+ ','.join(iss2iss) +"]\n")
    output.write("MEM = ["+ ','.join(MEM) +"]\n")
    output.write("Fetch_wait = ["+ ','.join(Fetch_wait) +"]\n")
    output.write("Issue_wait = ["+ ','.join(Issue_wait) +"]\n")
    output.write("OC_wait = ["+ ','.join(OC_wait) +"]\n")
    output.write("EX_wait = ["+ ','.join(EX_wait) +"]\n")
    output.write("WB_wait = ["+ ','.join(WB_wait) +"]\n")
    output.write("barrier = ["+ ','.join(barrier) +"]\n")
    output.write("global_access = ["+ ','.join(global_access) +"]\n")
    output.write("ALU = ["+ ','.join(ALU) +"]\n")
    output.write("INTP = ["+ ','.join(INTP) +"]\n")
    output.write("ret = ["+ ','.join(ret) +"]\n")
    output.write("warp_schedule_latency = ["+ ','.join(warp_schedule_latency) +"]\n")
    output.write("icnt = ["+ ','.join(icnt) +"]\n")
    output.write("L1 = ["+ ','.join(L1) +"]\n")
    output.write("L2 = ["+ ','.join(L2) +"]\n")
    output.write("control = ["+ ','.join(control) +"]\n")
    output.write(make_stack(sid))


    if box_printed == 0 and len(undefined) > 0:
        output.write("plt.legend(loc=\"lower left\",bbox_to_anchor=(1.02,0.3))\n")
        box_printed = 1
    output.write("\n\n")
    

    del undefined[:]
    del FD[:]
    del Iss[:]
    del OC[:]
    del EX[:]
    del WB[:]
    del DataDepen[:]
    del ibuffer[:]
    del iss2iss[:]
    del MEM[:]
    del Fetch_wait[:]
    del Issue_wait[:]
    del OC_wait[:]
    del EX_wait[:]
    del WB_wait[:]
    del barrier[:]
    del warps[:]
    del global_access[:]
    del ALU[:]
    del INTP[:]
    del ret[:]
    del warp_schedule_latency[:]
    del icnt[:]
    del L1[:]
    del L2[:]
    del control[:]
    sid = sid + 1
            

    # To seprate kernel to kernel
    output.write("store" + str(sid) +"_x = [element + w +length_stack for element in range("+str(len(undefined))+")]\n")
    output.write("length_stack = length_stack + "+str(len(undefined))+" + w + 1\n")

    output.write("\n")
    output.write("undefined = [0]\n")
    output.write("FD = [0]\n")
    output.write("Iss = [0]\n")
    output.write("OC = [0]\n")
    output.write("EX = [0]\n")
    output.write("WB = [0]\n")
    output.write("DataDepen = [0]\n")
    output.write("ibuffer = [0]\n")
    output.write("iss2iss = [0]\n")
    output.write("MEM = [0]\n")
    output.write("Fetch_wait = [0]\n")
    output.write("Issue_wait = [0]\n")
    output.write("OC_wait = [0]\n")
    output.write("EX_wait = [0]\n")
    output.write("WB_wait = [0]\n")
    output.write("barrier = [0]\n")
    output.write("global_access = [0]\n")
    output.write("ALU = [0]\n")
    output.write("INTP = [0]\n")
    output.write("ret = [0]\n")
    output.write("warp_schedule_latency = [0]\n")
    output.write("icnt = [0]\n")
    output.write("L1 = [0]\n")
    output.write("L2 = [0]\n")
    output.write("control = [0]\n")
    output.write(make_stack(sid))
    

    
    

g_warp_data = ""
#output.write("warp = [")
temp_num = 0
for warp_num in g_warps:
    g_warp_data += "\"" + str(temp_num) + "\""+','
    temp_num += 1
g_warp_data = g_warp_data[:-1]
g_warp_data += "]"



#output.write(g_warp_data)
output.write("#plt.show()\nplt.savefig('out_longest.png')\n")
#output.write("plt.savefig('out.png')\n")

output.close()

input.close()

     