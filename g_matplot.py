

def make_stack(sid):
    output_str = ""
    output_str = "plt.bar(store"+ str(sid) +"_x,undefined,label=\"undefined\",color='white')\n"
    output_str = output_str + "plt.bar(store"+ str(sid) +"_x,FD,bottom=np.array(undefined),label=\"FD\",color='brown')\n"
    output_str = output_str + "plt.bar(store"+ str(sid) +"_x,OC,bottom=np.array(undefined)+np.array(FD)+np.array(Iss),label=\"OC\",color='green')\n"
    output_str = output_str + "plt.bar(store"+ str(sid) +"_x,EX,bottom=np.array(undefined)+np.array(FD)+np.array(Iss)+np.array(OC),label=\"EX\",color='red')\n"
    output_str = output_str + "plt.bar(store"+ str(sid) +"_x,WB,bottom=np.array(undefined)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label=\"WB\",color='purple')\n"
    output_str = output_str + "plt.bar(store"+ str(sid) +"_x,DataDepen,bottom=np.array(undefined)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label=\"DataDepen\",color='gray')\n"
    output_str = output_str + "plt.bar(store"+ str(sid) +"_x,ibuffer,bottom=np.array(undefined)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label=\"ibuffer\",color='olive')\n"
    output_str = output_str + "plt.bar(store"+ str(sid) +"_x,iss2iss,bottom=np.array(undefined)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label=\"iss2iss\",color='cyan')\n"
    output_str = output_str + "plt.bar(store"+ str(sid) +"_x,MEM,bottom=np.array(undefined)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label=\"L1L2\",color='yellow')\n"
    output_str = output_str + "plt.bar(store"+ str(sid) +"_x,Fetch_wait,bottom=np.array(undefined)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label=\"Fetch_wait\",color='silver')\n"
    output_str = output_str + "plt.bar(store"+ str(sid) +"_x,Issue_wait,bottom=np.array(undefined)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label=\"Issue_wait\",color='navy')\n"
    output_str = output_str + "plt.bar(store"+ str(sid) +"_x,barrier,bottom=np.array(undefined)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label=\"barrier\",color='pink')\n"
    output_str = output_str + "plt.bar(store"+ str(sid) +"_x,global_access,bottom=np.array(undefined)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label=\"global_access\",color='orange')\n"
    output_str = output_str + "plt.bar(store"+ str(sid) +"_x,ALU,bottom=np.array(undefined)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label=\"ALU\",color='purple')\n"
    output_str = output_str + "plt.bar(store"+ str(sid) +"_x,INTP,bottom=np.array(undefined)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label=\"INTP\",color='greenyellow')\n"
    output_str = output_str + "plt.bar(store"+ str(sid) +"_x,ret,bottom=np.array(undefined)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label=\"ret\",color='black')\n"
    output_str = output_str + "plt.bar(store"+ str(sid) +"_x,warp_schedule_latency,bottom=np.array(undefined)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(ret),label=\"warp_schedule_latency\",color='cyan')\n"
    return output_str



output = open("./data_out.py", "w")
input = open("./data_in","r")
output.write("import numpy as np\nimport matplotlib.pyplot as plt\nlength_stack = 0\n")
output.write("d = 8\nw = 0.8\nall_store_x = []\n")

output.write("plt.figure(figsize=(100,9))\n")


priv_start_cycle = 0
pc = 0
warp_id = 0
sid = 0
inst_num = 0

warps = []
undefined = []
FD = []
Iss = []
OC = []
EX = []
WB = []
DataDepen = []
ibuffer = []
iss2iss = []
MEM = []
Fetch_wait = []
Issue_wait = []
OC_wait = []
EX_wait = []
WB_wait = []
barrier = []
global_access = []
ALU = []
INTP = []
ret = []
warp_schedule_latency = []



kernel = [None] * 32
# if error with this arr, expend kernel arr legnth
for temp in range(0,32):
    kernel[temp] = [None] * 100 #number of sid
    for sid in range(0,100):
        kernel[temp][sid] = [None] * 64 # # of sid

g_warps = []
g_undefined = []
g_FD = []
g_Iss = []
g_OC = []
g_EX = []
g_WB = []
g_DataDepen = []
g_ibuffer = []
g_iss2iss = []
g_MEM = []
g_Fetch_wait = []
g_Issue_wait = []
g_OC_wait = []
g_EX_wait = []
g_WB_wait = []
g_barrier = []

priv_ker_id = 0

while True:
     ##READ line
    line = input.readline()
    if not line: break
    latencys = line.split(' ')
    #print(line)
    if priv_ker_id < int(latencys[0]) :
        priv_ker_id = int(latencys[0])
        # kernel id/TB id/dy_warp id
    ThreadBlock = kernel[priv_ker_id]

    if ThreadBlock[int(latencys[2])][int(latencys[1])]==None:
        ThreadBlock[int(latencys[2])][int(latencys[1])] = [line[(line.find(":")+1):]]
    else:
        ThreadBlock[int(latencys[2])][int(latencys[1])].append(line[(line.find(":")+1):]) 
    
    #print(kernel[int(latencys[0])])
    # latencys[0] = sid, 

    
    
    g_undefined.append(latencys[3])
    g_FD.append(latencys[4])
    g_Iss.append(latencys[5])
    g_OC.append(latencys[6])
    g_EX.append(latencys[7])
    g_WB.append(latencys[8])
    g_DataDepen.append(latencys[9])
    g_ibuffer.append(latencys[10])
    g_iss2iss.append(latencys[11])
    g_MEM.append(latencys[12])
    g_Fetch_wait.append(latencys[13])
    g_Issue_wait.append(latencys[14])
    g_OC_wait.append(latencys[15])
    g_EX_wait.append(latencys[16])
    g_WB_wait.append(latencys[17])
    g_barrier.append(latencys[18])

print(kernel)
box_printed = 0
once_printed = 0
for ele in kernel:
    if (ele == None) or (ele == [None] * 32): continue
    for tb in ele:
        if (tb == None) or (tb == [None] * 100): continue
        if once_printed == 1:
    ##2
            output.write("store" + str(sid) +"_x = [element + w +length_stack for element in range("+str(len(undefined))+")]\n")
            output.write("length_stack = length_stack + "+str(len(undefined))+" + w + 1\n")
        
            output.write("\n")
            output.write("undefined = [0]\n")
            output.write("FD = [0]\n")
            output.write("Iss = [0]\n")
            output.write("OC = [0]\n")
            output.write("EX = [0]\n")
            output.write("WB = [0]\n")
            output.write("DataDepen = [0]\n")
            output.write("ibuffer = [0]\n")
            output.write("iss2iss = [0]\n")
            output.write("MEM = [0]\n")
            output.write("Fetch_wait = [0]\n")
            output.write("Issue_wait = [0]\n")
            output.write("OC_wait = [0]\n")
            output.write("EX_wait = [0]\n")
            output.write("WB_wait = [0]\n")
            output.write("barrier = [0]\n")
            output.write("global_access = [0]\n")
            output.write("ALU = [0]\n")
            output.write("INTP = [0]\n")
            output.write("ret = [0]\n")
            output.write("warp_schedule_latency = [0]\n")
            output.write(make_stack(sid))
            
            if box_printed == 0:
                output.write("plt.legend(loc=\"lower left\",bbox_to_anchor=(1.02,0.6))\n")
                box_printed = 1
            output.write("\n\n")
            sid = sid + 1
        once_printed = 0
        for warp in tb:
            if (warp == None) or (warp == [None] * 64): continue
            for line in warp: #same sid, same TB same kernel
                #print(line)
                if not line: break
                if line == None: continue
                latencys = line.split(' ')
                undefined.append(latencys[1])
                FD.append(latencys[2])
                Iss.append(latencys[3])
                OC.append(latencys[4])
                EX.append(latencys[5])
                WB.append(latencys[6])
                DataDepen.append(latencys[7])
                ibuffer.append(latencys[8])
                iss2iss.append(latencys[9])
                MEM.append(latencys[10])
                Fetch_wait.append(latencys[11])
                Issue_wait.append(latencys[12])
                OC_wait.append(latencys[13])
                EX_wait.append(latencys[14])
                WB_wait.append(latencys[15])
                barrier.append(latencys[16])
                global_access.append(latencys[17])
                ALU.append(latencys[18])
                INTP.append(latencys[19])
                ret.append(latencys[20])
                warp_schedule_latency.append(latencys[21])
            
            if(undefined == [None]): continue
            output.write("store" + str(sid) +"_x = [element + w +length_stack for element in range("+str(len(undefined))+")]\n")
            output.write("length_stack = length_stack + "+str(len(undefined))+" + w + 1\n")
            """if sid == 0:
                output.write("all_store_x = store1_x\n")
                
            else:
                output.write("for ele in store"+str(sid)+"_x:\n    all_store_x.append(ele)\n")
            """    

            output.write("\n")
            output.write("undefined = ["+ ','.join(undefined) +"]\n")
            output.write("FD = ["+ ','.join(FD) +"]\n")
            output.write("Iss = ["+ ','.join(Iss) +"]\n")
            output.write("OC = ["+ ','.join(OC) +"]\n")
            output.write("EX = ["+ ','.join(EX) +"]\n")
            output.write("WB = ["+ ','.join(WB) +"]\n")
            output.write("DataDepen = ["+ ','.join(DataDepen) +"]\n")
            output.write("ibuffer = ["+ ','.join(ibuffer) +"]\n")
            output.write("iss2iss = ["+ ','.join(iss2iss) +"]\n")
            output.write("MEM = ["+ ','.join(MEM) +"]\n")
            output.write("Fetch_wait = ["+ ','.join(Fetch_wait) +"]\n")
            output.write("Issue_wait = ["+ ','.join(Issue_wait) +"]\n")
            output.write("OC_wait = ["+ ','.join(OC_wait) +"]\n")
            output.write("EX_wait = ["+ ','.join(EX_wait) +"]\n")
            output.write("WB_wait = ["+ ','.join(WB_wait) +"]\n")
            output.write("barrier = ["+ ','.join(barrier) +"]\n")
            output.write("global_access = ["+ ','.join(global_access) +"]\n")
            output.write("ALU = ["+ ','.join(ALU) +"]\n")
            output.write("INTP = ["+ ','.join(INTP) +"]\n")
            output.write("ret = ["+ ','.join(ret) +"]\n")
            output.write("warp_schedule_latency = ["+ ','.join(warp_schedule_latency) +"]\n")
            output.write(make_stack(sid))
            
            if box_printed == 0:
                output.write("plt.legend(loc=\"lower left\",bbox_to_anchor=(1.02,0.6))\n")
                box_printed = 1
            output.write("\n\n")
            once_printed = 1

            del undefined[:]
            del FD[:]
            del Iss[:]
            del OC[:]
            del EX[:]
            del WB[:]
            del DataDepen[:]
            del ibuffer[:]
            del iss2iss[:]
            del MEM[:]
            del Fetch_wait[:]
            del Issue_wait[:]
            del OC_wait[:]
            del EX_wait[:]
            del WB_wait[:]
            del barrier[:]
            del warps[:]
            del global_access[:]
            del ALU[:]
            del INTP[:]
            del ret[:]
            del warp_schedule_latency[:]
            

            sid = sid + 1

    """output.write("store" + str(sid) +"_x = [element + w +length_stack for element in range("+str(len(undefined))+")]\n")
    output.write("length_stack = length_stack + "+str(len(undefined))+" + w + 1\n")

    output.write("\n")
    output.write("undefined = ["+ ','.join(undefined) +"]\n")
    output.write("FD = ["+ ','.join(FD) +"]\n")
    output.write("Iss = ["+ ','.join(Iss) +"]\n")
    output.write("OC = ["+ ','.join(OC) +"]\n")
    output.write("EX = ["+ ','.join(EX) +"]\n")
    output.write("WB = ["+ ','.join(WB) +"]\n")
    output.write("DataDepen = ["+ ','.join(DataDepen) +"]\n")
    output.write("ibuffer = ["+ ','.join(ibuffer) +"]\n")
    output.write("iss2iss = ["+ ','.join(iss2iss) +"]\n")
    output.write("MEM = ["+ ','.join(MEM) +"]\n")
    output.write("Fetch_wait = ["+ ','.join(Fetch_wait) +"]\n")
    output.write("Issue_wait = ["+ ','.join(Issue_wait) +"]\n")
    output.write("OC_wait = ["+ ','.join(OC_wait) +"]\n")
    output.write("EX_wait = ["+ ','.join(EX_wait) +"]\n")
    output.write("WB_wait = ["+ ','.join(WB_wait) +"]\n")
    output.write("barrier = ["+ ','.join(barrier) +"]\n")
    output.write("global_access = ["+ ','.join(global_access) +"]\n")
    output.write("ALU = ["+ ','.join(ALU) +"]\n")
    output.write("INTP = ["+ ','.join(INTP) +"]\n")
    output.write("ret = ["+ ','.join(ret) +"]\n")
    output.write("warp_schedule_latency = ["+ ','.join(warp_schedule_latency) +"]\n")
    output.write(make_stack(sid))
    
    if box_printed == 0:
        output.write("plt.legend(loc=\"lower left\",bbox_to_anchor=(1.02,0.6))\n")
        box_printed = 1 
    output.write("\n\n")
    sid = sid + 1"""

    """if once_printed == 1:
    ##2
        output.write("store" + str(sid) +"_x = [element + w +length_stack for element in range("+str(len(undefined))+")]\n")
        output.write("length_stack = length_stack + "+str(len(undefined))+" + w + 1\n")
    
        output.write("\n")
        output.write("undefined = [0]\n")
        output.write("FD = [0]\n")
        output.write("Iss = [0]\n")
        output.write("OC = [0]\n")
        output.write("EX = [0]\n")
        output.write("WB = [0]\n")
        output.write("DataDepen = [0]\n")
        output.write("ibuffer = [0]\n")
        output.write("iss2iss = [0]\n")
        output.write("MEM = [0]\n")
        output.write("Fetch_wait = [0]\n")
        output.write("Issue_wait = [0]\n")
        output.write("OC_wait = [0]\n")
        output.write("EX_wait = [0]\n")
        output.write("WB_wait = [0]\n")
        output.write("barrier = [0]\n")
        output.write("global_access = [0]\n")
        output.write("ALU = [0]\n")
        output.write("INTP = [0]\n")
        output.write("ret = [0]\n")
        output.write("warp_schedule_latency = [0]\n")
        output.write(make_stack(sid))
        
        if box_printed == 0:
            output.write("plt.legend(loc=\"lower left\",bbox_to_anchor=(1.02,0.6))\n")
            box_printed = 1
        output.write("\n\n")
        sid = sid + 1
        once_printed = 0"""

g_warp_data = ""
#output.write("warp = [")
temp_num = 0
for warp_num in g_warps:
    g_warp_data += "\"" + str(temp_num) + "\""+','
    temp_num += 1
g_warp_data = g_warp_data[:-1]
g_warp_data += "]"



#output.write(g_warp_data)
output.write("#plt.show()\nplt.savefig('out.png')\n")
#output.write("plt.savefig('out.png')\n")

output.close()

input.close()

     