import numpy as np
import matplotlib.pyplot as plt
length_stack = 0
d = 8
w = 0.8
all_store_x = []
plt.figure(figsize=(100,9))
store0_x = [element + w +length_stack for element in range(4)]
length_stack = length_stack + 4 + w + 1

undefined = [0,0,0,0]
FD = [96,96,96,111]
Iss = [0,0,0,0]
OC = [32,224,256,350]
EX = [96,96,96,711]
WB = [0,0,0,0]
DataDepen = [32,32,32,62]
ibuffer = [0,0,0,0]
iss2iss = [64,64,64,64]
MEM = [0,0,0,0]
Fetch_wait = [0,0,0,0]
Issue_wait = [32,0,0,32]
OC_wait = [0,0,0,0]
EX_wait = [0,0,0,0]
WB_wait = [0,0,0,0]
barrier = [0,0,0,0]
global_access = [0,0,0,0]
ALU = [640,384,384,789]
INTP = [320,288,288,1248]
ret = [32,32,32,32]
warp_schedule_latency = [1,1,1,1]
icnt = [0,0,0,450]
L1 = [0,160,160,96]
L2 = [0,0,0,2625]
control = [32,32,32,5846]
plt.bar(store0_x,undefined,label="undefined",color='white')
plt.bar(store0_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store0_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store0_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store0_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store0_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store0_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store0_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store0_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store0_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store0_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store0_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store0_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store0_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store0_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store0_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store0_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store0_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store0_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store0_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store0_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')
plt.legend(loc="lower left",bbox_to_anchor=(1.02,0.3))


store1_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store1_x,undefined,label="undefined",color='white')
plt.bar(store1_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store1_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store1_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store1_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store1_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store1_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store1_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store1_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store1_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store1_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store1_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store1_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store1_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store1_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store1_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store1_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store1_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store1_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store1_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store1_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store2_x = [element + w +length_stack for element in range(8)]
length_stack = length_stack + 8 + w + 1

undefined = [0,0,0,0,0,0,0,0]
FD = [128,128,128,128,128,128,128,96]
Iss = [0,0,0,0,0,0,0,0]
OC = [92,212,212,2432,1712,1202,1622,662]
EX = [276,276,276,276,276,276,276,366]
WB = [0,0,0,0,0,0,0,0]
DataDepen = [182,182,182,182,182,182,182,242]
ibuffer = [0,0,0,0,0,0,0,0]
iss2iss = [94,94,94,94,64,64,64,96]
MEM = [0,0,0,0,0,0,0,0]
Fetch_wait = [0,0,0,0,0,0,0,0]
Issue_wait = [800,800,832,960,962,960,960,892]
OC_wait = [0,0,0,0,0,0,0,0]
EX_wait = [0,0,0,0,0,0,0,0]
WB_wait = [0,0,0,0,0,0,0,0]
barrier = [0,0,0,0,0,0,0,0]
global_access = [0,0,0,0,0,0,0,0]
ALU = [1272,1304,1304,1304,1304,1272,1304,1255]
INTP = [3950,3950,3950,4010,3950,3950,4010,4595]
ret = [2800,2785,2770,2755,2740,2710,2695,2279]
warp_schedule_latency = [2,2,2,1,1,1,1,2]
icnt = [1800,0,0,0,900,0,0,120]
L1 = [0,16470,16470,14130,8400,17220,16740,8910]
L2 = [14790,0,0,0,7320,0,0,14925]
control = [1512,1512,1512,1508,1650,1658,1658,9505]
plt.bar(store2_x,undefined,label="undefined",color='white')
plt.bar(store2_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store2_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store2_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store2_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store2_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store2_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store2_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store2_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store2_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store2_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store2_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store2_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store2_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store2_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store2_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store2_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store2_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store2_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store2_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store2_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store3_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store3_x,undefined,label="undefined",color='white')
plt.bar(store3_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store3_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store3_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store3_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store3_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store3_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store3_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store3_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store3_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store3_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store3_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store3_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store3_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store3_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store3_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store3_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store3_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store3_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store3_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store3_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store4_x = [element + w +length_stack for element in range(4)]
length_stack = length_stack + 4 + w + 1

undefined = [0,0,0,0]
FD = [96,96,96,110]
Iss = [0,0,0,0]
OC = [32,224,256,348]
EX = [96,96,96,670]
WB = [0,0,0,0]
DataDepen = [32,32,32,60]
ibuffer = [0,0,0,0]
iss2iss = [64,64,64,64]
MEM = [0,0,0,0]
Fetch_wait = [0,0,0,0]
Issue_wait = [32,0,0,32]
OC_wait = [0,0,0,0]
EX_wait = [0,0,0,0]
WB_wait = [0,0,0,0]
barrier = [0,0,0,0]
global_access = [0,0,0,0]
ALU = [640,384,384,762]
INTP = [320,288,288,1184]
ret = [7124,7123,7122,6770]
warp_schedule_latency = [1,1,1,1]
icnt = [0,0,0,392]
L1 = [0,160,160,96]
L2 = [0,0,0,2464]
control = [32,32,32,6170]
plt.bar(store4_x,undefined,label="undefined",color='white')
plt.bar(store4_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store4_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store4_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store4_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store4_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store4_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store4_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store4_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store4_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store4_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store4_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store4_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store4_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store4_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store4_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store4_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store4_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store4_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store4_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store4_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store5_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store5_x,undefined,label="undefined",color='white')
plt.bar(store5_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store5_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store5_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store5_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store5_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store5_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store5_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store5_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store5_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store5_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store5_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store5_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store5_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store5_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store5_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store5_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store5_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store5_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store5_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store5_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store6_x = [element + w +length_stack for element in range(8)]
length_stack = length_stack + 8 + w + 1

undefined = [0,0,0,0,0,0,0,0]
FD = [128,128,128,128,128,128,128,96]
Iss = [0,0,0,0,0,0,0,0]
OC = [88,200,116,2160,1488,3084,1404,592]
EX = [264,264,180,264,264,264,264,348]
WB = [0,0,0,0,0,0,0,0]
DataDepen = [172,172,102,172,172,172,172,228]
ibuffer = [0,0,0,0,0,0,0,0]
iss2iss = [92,92,78,92,64,64,64,96]
MEM = [0,0,0,0,0,0,0,0]
Fetch_wait = [0,0,0,0,0,0,0,0]
Issue_wait = [800,800,832,960,964,960,960,888]
OC_wait = [0,0,0,0,0,0,0,0]
EX_wait = [0,0,0,0,0,0,0,0]
WB_wait = [0,0,0,0,0,0,0,0]
barrier = [0,0,0,0,0,0,0,0]
global_access = [0,0,0,0,0,0,0,0]
ALU = [1232,1264,1166,1264,1264,1232,1264,1214]
INTP = [3708,3708,2168,3764,3708,3708,3764,4310]
ret = [2799,2785,2771,2757,2728,2700,2686,2270]
warp_schedule_latency = [2,2,2,1,1,1,1,2]
icnt = [1568,0,0,0,1512,0,0,112]
L1 = [0,15428,7714,13356,7700,14084,15708,8484]
L2 = [13972,0,0,0,6384,0,0,13902]
control = [2936,2936,12540,2928,3168,3184,3184,11682]
plt.bar(store6_x,undefined,label="undefined",color='white')
plt.bar(store6_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store6_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store6_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store6_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store6_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store6_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store6_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store6_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store6_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store6_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store6_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store6_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store6_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store6_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store6_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store6_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store6_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store6_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store6_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store6_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store7_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store7_x,undefined,label="undefined",color='white')
plt.bar(store7_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store7_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store7_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store7_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store7_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store7_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store7_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store7_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store7_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store7_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store7_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store7_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store7_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store7_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store7_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store7_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store7_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store7_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store7_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store7_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store8_x = [element + w +length_stack for element in range(4)]
length_stack = length_stack + 4 + w + 1

undefined = [0,0,0,0]
FD = [96,96,96,109]
Iss = [0,0,0,0]
OC = [32,224,256,346]
EX = [96,96,96,629]
WB = [0,0,0,0]
DataDepen = [32,32,32,58]
ibuffer = [0,0,0,0]
iss2iss = [64,64,64,64]
MEM = [0,0,0,0]
Fetch_wait = [0,0,0,0]
Issue_wait = [32,0,0,32]
OC_wait = [0,0,0,0]
EX_wait = [0,0,0,0]
WB_wait = [0,0,0,0]
barrier = [0,0,0,0]
global_access = [0,0,0,0]
ALU = [640,384,384,735]
INTP = [320,288,288,1120]
ret = [7124,7123,7122,6772]
warp_schedule_latency = [1,1,1,1]
icnt = [0,0,0,338]
L1 = [0,160,160,96]
L2 = [0,0,0,2301]
control = [32,32,32,6492]
plt.bar(store8_x,undefined,label="undefined",color='white')
plt.bar(store8_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store8_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store8_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store8_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store8_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store8_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store8_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store8_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store8_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store8_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store8_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store8_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store8_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store8_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store8_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store8_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store8_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store8_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store8_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store8_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store9_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store9_x,undefined,label="undefined",color='white')
plt.bar(store9_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store9_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store9_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store9_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store9_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store9_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store9_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store9_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store9_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store9_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store9_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store9_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store9_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store9_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store9_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store9_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store9_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store9_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store9_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store9_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store10_x = [element + w +length_stack for element in range(8)]
length_stack = length_stack + 8 + w + 1

undefined = [0,0,0,0,0,0,0,0]
FD = [128,128,128,128,128,128,128,96]
Iss = [0,0,0,0,0,0,0,0]
OC = [32,84,188,1462,1462,630,2008,240]
EX = [128,252,252,252,252,252,252,330]
WB = [0,0,0,0,0,0,0,0]
DataDepen = [32,162,162,162,162,162,162,214]
ibuffer = [0,0,0,0,0,0,0,0]
iss2iss = [64,90,90,90,90,64,64,96]
MEM = [0,0,0,0,0,0,0,0]
Fetch_wait = [0,0,0,0,0,0,0,0]
Issue_wait = [832,800,800,966,960,934,960,884]
OC_wait = [0,0,0,0,0,0,0,0]
EX_wait = [0,0,0,0,0,0,0,0]
WB_wait = [0,0,0,0,0,0,0,0]
barrier = [0,0,0,0,0,0,0,0]
global_access = [0,0,0,0,0,0,0,0]
ALU = [1042,1192,1224,1224,1224,1224,1192,1173]
INTP = [606,3466,3466,3466,3518,3466,3466,3973]
ret = [3485,2838,2825,2812,2799,2786,2760,2344]
warp_schedule_latency = [2,2,2,1,1,1,1,2]
icnt = [0,1352,0,0,0,676,0,104]
L1 = [0,0,13338,11960,11960,6994,12506,7020]
L2 = [0,12090,0,0,0,6162,0,12454]
control = [208,4128,4128,4130,4116,4364,4382,13000]
plt.bar(store10_x,undefined,label="undefined",color='white')
plt.bar(store10_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store10_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store10_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store10_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store10_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store10_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store10_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store10_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store10_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store10_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store10_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store10_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store10_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store10_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store10_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store10_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store10_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store10_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store10_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store10_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store11_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store11_x,undefined,label="undefined",color='white')
plt.bar(store11_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store11_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store11_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store11_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store11_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store11_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store11_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store11_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store11_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store11_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store11_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store11_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store11_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store11_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store11_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store11_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store11_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store11_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store11_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store11_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store12_x = [element + w +length_stack for element in range(4)]
length_stack = length_stack + 4 + w + 1

undefined = [0,0,0,0]
FD = [96,96,96,108]
Iss = [0,0,0,0]
OC = [32,224,256,344]
EX = [96,96,96,588]
WB = [0,0,0,0]
DataDepen = [32,32,32,56]
ibuffer = [0,0,0,0]
iss2iss = [64,64,64,64]
MEM = [0,0,0,0]
Fetch_wait = [0,0,0,0]
Issue_wait = [32,0,0,32]
OC_wait = [0,0,0,0]
EX_wait = [0,0,0,0]
WB_wait = [0,0,0,0]
barrier = [0,0,0,0]
global_access = [0,0,0,0]
ALU = [640,384,384,708]
INTP = [320,288,288,1056]
ret = [7124,7123,7122,6774]
warp_schedule_latency = [1,1,1,1]
icnt = [0,0,0,288]
L1 = [0,160,160,96]
L2 = [0,0,0,2136]
control = [32,32,32,6812]
plt.bar(store12_x,undefined,label="undefined",color='white')
plt.bar(store12_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store12_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store12_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store12_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store12_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store12_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store12_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store12_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store12_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store12_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store12_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store12_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store12_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store12_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store12_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store12_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store12_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store12_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store12_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store12_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store13_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store13_x,undefined,label="undefined",color='white')
plt.bar(store13_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store13_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store13_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store13_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store13_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store13_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store13_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store13_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store13_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store13_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store13_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store13_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store13_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store13_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store13_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store13_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store13_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store13_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store13_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store13_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store14_x = [element + w +length_stack for element in range(8)]
length_stack = length_stack + 8 + w + 1

undefined = [0,0,0,0,0,0,0,0]
FD = [128,128,128,128,128,128,128,96]
Iss = [0,0,0,0,0,0,0,0]
OC = [32,56,176,1256,1256,416,1712,344]
EX = [128,168,240,240,240,240,240,312]
WB = [0,0,0,0,0,0,0,0]
DataDepen = [32,92,152,152,152,152,152,200]
ibuffer = [0,0,0,0,0,0,0,0]
iss2iss = [64,76,88,88,88,64,64,96]
MEM = [0,0,0,0,0,0,0,0]
Fetch_wait = [0,0,0,0,0,0,0,0]
Issue_wait = [832,800,800,968,960,936,960,880]
OC_wait = [0,0,0,0,0,0,0,0]
EX_wait = [0,0,0,0,0,0,0,0]
WB_wait = [0,0,0,0,0,0,0,0]
barrier = [0,0,0,0,0,0,0,0]
global_access = [0,0,0,0,0,0,0,0]
ALU = [1016,1068,1184,1184,1184,1184,1152,1132]
INTP = [584,1904,3224,3224,3272,3224,3224,3692]
ret = [3485,2838,2826,2814,2802,2778,2754,2338]
warp_schedule_latency = [2,2,2,1,1,1,1,2]
icnt = [0,576,0,0,0,0,0,672]
L1 = [0,0,12336,11160,11160,12936,11664,0]
L2 = [0,5640,0,0,0,0,0,17328]
control = [256,13268,5480,5472,5464,5776,5792,15024]
plt.bar(store14_x,undefined,label="undefined",color='white')
plt.bar(store14_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store14_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store14_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store14_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store14_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store14_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store14_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store14_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store14_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store14_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store14_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store14_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store14_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store14_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store14_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store14_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store14_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store14_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store14_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store14_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store15_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store15_x,undefined,label="undefined",color='white')
plt.bar(store15_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store15_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store15_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store15_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store15_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store15_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store15_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store15_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store15_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store15_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store15_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store15_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store15_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store15_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store15_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store15_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store15_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store15_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store15_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store15_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store16_x = [element + w +length_stack for element in range(4)]
length_stack = length_stack + 4 + w + 1

undefined = [0,0,0,0]
FD = [96,96,96,107]
Iss = [0,0,0,0]
OC = [32,224,256,342]
EX = [96,96,96,547]
WB = [0,0,0,0]
DataDepen = [32,32,32,54]
ibuffer = [0,0,0,0]
iss2iss = [64,64,64,64]
MEM = [0,0,0,0]
Fetch_wait = [0,0,0,0]
Issue_wait = [32,0,0,32]
OC_wait = [0,0,0,0]
EX_wait = [0,0,0,0]
WB_wait = [0,0,0,0]
barrier = [0,0,0,0]
global_access = [0,0,0,0]
ALU = [640,384,384,681]
INTP = [320,288,288,992]
ret = [7124,7123,7122,6776]
warp_schedule_latency = [1,1,1,1]
icnt = [0,0,0,242]
L1 = [0,160,160,96]
L2 = [0,0,0,1969]
control = [32,32,32,7130]
plt.bar(store16_x,undefined,label="undefined",color='white')
plt.bar(store16_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store16_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store16_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store16_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store16_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store16_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store16_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store16_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store16_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store16_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store16_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store16_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store16_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store16_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store16_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store16_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store16_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store16_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store16_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store16_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store17_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store17_x,undefined,label="undefined",color='white')
plt.bar(store17_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store17_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store17_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store17_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store17_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store17_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store17_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store17_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store17_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store17_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store17_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store17_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store17_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store17_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store17_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store17_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store17_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store17_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store17_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store17_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store18_x = [element + w +length_stack for element in range(8)]
length_stack = length_stack + 8 + w + 1

undefined = [0,0,0,0,0,0,0,0]
FD = [128,128,128,128,128,128,128,96]
Iss = [0,0,0,0,0,0,0,0]
OC = [32,32,120,164,582,736,120,208]
EX = [96,96,228,228,228,228,228,294]
WB = [0,0,0,0,0,0,0,0]
DataDepen = [32,32,142,142,142,142,142,186]
ibuffer = [0,0,0,0,0,0,0,0]
iss2iss = [64,64,86,86,86,86,86,118]
MEM = [0,0,0,0,0,0,0,0]
Fetch_wait = [0,0,0,0,0,0,0,0]
Issue_wait = [800,832,800,938,970,960,938,854]
OC_wait = [0,0,0,0,0,0,0,0]
EX_wait = [0,0,0,0,0,0,0,0]
WB_wait = [0,0,0,0,0,0,0,0]
barrier = [0,0,0,0,0,0,0,0]
global_access = [0,0,0,0,0,0,0,0]
ALU = [958,990,1144,1112,1144,1144,1144,1091]
INTP = [562,562,2982,2982,2982,3026,2982,3411]
ret = [3488,3486,2868,2858,2847,2836,2814,2398]
warp_schedule_latency = [2,2,2,1,1,1,1,2]
icnt = [0,0,968,0,0,0,0,528]
L1 = [0,0,0,10582,10142,9988,11132,5588]
L2 = [0,0,9702,0,0,0,0,9350]
control = [304,304,6516,6518,6508,6506,6748,16132]
plt.bar(store18_x,undefined,label="undefined",color='white')
plt.bar(store18_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store18_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store18_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store18_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store18_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store18_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store18_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store18_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store18_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store18_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store18_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store18_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store18_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store18_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store18_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store18_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store18_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store18_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store18_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store18_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store19_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store19_x,undefined,label="undefined",color='white')
plt.bar(store19_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store19_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store19_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store19_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store19_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store19_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store19_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store19_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store19_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store19_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store19_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store19_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store19_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store19_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store19_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store19_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store19_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store19_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store19_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store19_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store20_x = [element + w +length_stack for element in range(4)]
length_stack = length_stack + 4 + w + 1

undefined = [0,0,0,0]
FD = [96,96,96,106]
Iss = [0,0,0,0]
OC = [32,224,256,340]
EX = [96,96,96,506]
WB = [0,0,0,0]
DataDepen = [32,32,32,52]
ibuffer = [0,0,0,0]
iss2iss = [64,64,64,64]
MEM = [0,0,0,0]
Fetch_wait = [0,0,0,0]
Issue_wait = [32,0,0,32]
OC_wait = [0,0,0,0]
EX_wait = [0,0,0,0]
WB_wait = [0,0,0,0]
barrier = [0,0,0,0]
global_access = [0,0,0,0]
ALU = [640,384,384,654]
INTP = [320,288,288,928]
ret = [7124,7123,7122,6778]
warp_schedule_latency = [1,1,1,1]
icnt = [0,0,0,200]
L1 = [0,160,160,96]
L2 = [0,0,0,1800]
control = [32,32,32,7446]
plt.bar(store20_x,undefined,label="undefined",color='white')
plt.bar(store20_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store20_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store20_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store20_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store20_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store20_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store20_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store20_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store20_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store20_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store20_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store20_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store20_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store20_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store20_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store20_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store20_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store20_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store20_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store20_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store21_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store21_x,undefined,label="undefined",color='white')
plt.bar(store21_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store21_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store21_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store21_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store21_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store21_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store21_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store21_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store21_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store21_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store21_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store21_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store21_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store21_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store21_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store21_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store21_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store21_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store21_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store21_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store22_x = [element + w +length_stack for element in range(8)]
length_stack = length_stack + 8 + w + 1

undefined = [0,0,0,0,0,0,0,0]
FD = [128,128,128,128,128,128,128,96]
Iss = [0,0,0,0,0,0,0,0]
OC = [32,32,72,152,492,612,112,192]
EX = [96,96,156,216,216,216,216,276]
WB = [0,0,0,0,0,0,0,0]
DataDepen = [32,32,82,132,132,132,132,172]
ibuffer = [0,0,0,0,0,0,0,0]
iss2iss = [64,64,74,84,84,84,84,116]
MEM = [0,0,0,0,0,0,0,0]
Fetch_wait = [0,0,0,0,0,0,0,0]
Issue_wait = [800,832,800,940,972,960,940,852]
OC_wait = [0,0,0,0,0,0,0,0]
EX_wait = [0,0,0,0,0,0,0,0]
WB_wait = [0,0,0,0,0,0,0,0]
barrier = [0,0,0,0,0,0,0,0]
global_access = [0,0,0,0,0,0,0,0]
ALU = [932,964,1034,1072,1104,1104,1104,1050]
INTP = [540,540,1640,2740,2740,2780,2740,3130]
ret = [3488,3486,2868,2860,2850,2840,2810,2394]
warp_schedule_latency = [2,2,2,1,1,1,1,2]
icnt = [0,0,400,0,0,0,0,440]
L1 = [0,0,0,9620,9280,9160,10100,4980]
L2 = [0,0,4450,0,0,0,0,8670]
control = [352,352,13980,7796,7796,7800,8060,18010]
plt.bar(store22_x,undefined,label="undefined",color='white')
plt.bar(store22_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store22_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store22_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store22_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store22_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store22_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store22_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store22_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store22_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store22_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store22_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store22_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store22_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store22_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store22_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store22_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store22_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store22_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store22_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store22_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store23_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store23_x,undefined,label="undefined",color='white')
plt.bar(store23_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store23_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store23_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store23_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store23_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store23_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store23_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store23_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store23_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store23_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store23_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store23_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store23_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store23_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store23_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store23_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store23_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store23_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store23_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store23_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store24_x = [element + w +length_stack for element in range(4)]
length_stack = length_stack + 4 + w + 1

undefined = [0,0,0,0]
FD = [96,96,96,105]
Iss = [0,0,0,0]
OC = [32,224,256,338]
EX = [96,96,96,465]
WB = [0,0,0,0]
DataDepen = [32,32,32,50]
ibuffer = [0,0,0,0]
iss2iss = [64,64,64,64]
MEM = [0,0,0,0]
Fetch_wait = [0,0,0,0]
Issue_wait = [32,0,0,32]
OC_wait = [0,0,0,0]
EX_wait = [0,0,0,0]
WB_wait = [0,0,0,0]
barrier = [0,0,0,0]
global_access = [0,0,0,0]
ALU = [640,384,384,627]
INTP = [320,288,288,864]
ret = [7124,7123,7122,6780]
warp_schedule_latency = [1,1,1,1]
icnt = [0,0,0,162]
L1 = [0,160,160,96]
L2 = [0,0,0,1629]
control = [32,32,32,7760]
plt.bar(store24_x,undefined,label="undefined",color='white')
plt.bar(store24_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store24_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store24_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store24_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store24_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store24_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store24_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store24_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store24_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store24_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store24_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store24_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store24_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store24_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store24_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store24_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store24_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store24_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store24_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store24_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store25_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store25_x,undefined,label="undefined",color='white')
plt.bar(store25_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store25_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store25_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store25_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store25_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store25_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store25_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store25_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store25_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store25_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store25_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store25_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store25_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store25_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store25_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store25_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store25_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store25_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store25_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store25_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store26_x = [element + w +length_stack for element in range(8)]
length_stack = length_stack + 8 + w + 1

undefined = [0,0,0,0,0,0,0,0]
FD = [128,128,128,128,128,128,128,96]
Iss = [0,0,0,0,0,0,0,0]
OC = [32,32,32,140,140,140,446,86]
EX = [96,96,96,204,204,204,204,258]
WB = [0,0,0,0,0,0,0,0]
DataDepen = [32,32,32,122,122,122,122,158]
ibuffer = [0,0,0,0,0,0,0,0]
iss2iss = [64,64,64,64,64,64,64,114]
MEM = [0,0,0,0,0,0,0,0]
Fetch_wait = [0,0,0,0,0,0,0,0]
Issue_wait = [800,800,832,942,942,974,960,850]
OC_wait = [0,0,0,0,0,0,0,0]
EX_wait = [0,0,0,0,0,0,0,0]
WB_wait = [0,0,0,0,0,0,0,0]
barrier = [0,0,0,0,0,0,0,0]
global_access = [0,0,0,0,0,0,0,0]
ALU = [906,938,938,1032,1064,1064,1064,1009]
INTP = [518,518,518,2498,2498,2498,2534,2849]
ret = [3488,3487,3486,2889,2880,2871,2862,2492]
warp_schedule_latency = [2,2,2,1,1,1,1,2]
icnt = [0,0,0,360,0,0,0,684]
L1 = [0,0,0,0,8244,8244,7938,0]
L2 = [0,0,0,7884,0,0,0,10494]
control = [400,400,400,8738,8738,8738,8748,18250]
plt.bar(store26_x,undefined,label="undefined",color='white')
plt.bar(store26_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store26_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store26_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store26_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store26_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store26_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store26_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store26_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store26_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store26_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store26_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store26_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store26_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store26_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store26_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store26_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store26_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store26_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store26_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store26_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store27_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store27_x,undefined,label="undefined",color='white')
plt.bar(store27_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store27_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store27_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store27_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store27_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store27_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store27_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store27_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store27_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store27_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store27_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store27_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store27_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store27_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store27_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store27_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store27_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store27_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store27_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store27_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store28_x = [element + w +length_stack for element in range(4)]
length_stack = length_stack + 4 + w + 1

undefined = [0,0,0,0]
FD = [96,96,96,104]
Iss = [0,0,0,0]
OC = [32,224,256,336]
EX = [96,96,96,424]
WB = [0,0,0,0]
DataDepen = [32,32,32,48]
ibuffer = [0,0,0,0]
iss2iss = [64,64,64,64]
MEM = [0,0,0,0]
Fetch_wait = [0,0,0,0]
Issue_wait = [32,0,0,32]
OC_wait = [0,0,0,0]
EX_wait = [0,0,0,0]
WB_wait = [0,0,0,0]
barrier = [0,0,0,0]
global_access = [0,0,0,0]
ALU = [640,384,384,600]
INTP = [320,288,288,800]
ret = [7124,7123,7122,6782]
warp_schedule_latency = [1,1,1,1]
icnt = [0,0,0,128]
L1 = [0,160,160,96]
L2 = [0,0,0,1456]
control = [32,32,32,8072]
plt.bar(store28_x,undefined,label="undefined",color='white')
plt.bar(store28_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store28_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store28_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store28_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store28_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store28_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store28_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store28_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store28_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store28_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store28_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store28_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store28_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store28_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store28_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store28_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store28_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store28_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store28_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store28_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store29_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store29_x,undefined,label="undefined",color='white')
plt.bar(store29_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store29_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store29_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store29_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store29_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store29_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store29_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store29_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store29_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store29_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store29_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store29_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store29_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store29_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store29_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store29_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store29_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store29_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store29_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store29_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store30_x = [element + w +length_stack for element in range(8)]
length_stack = length_stack + 8 + w + 1

undefined = [0,0,0,0,0,0,0,0]
FD = [128,128,128,128,128,128,128,96]
Iss = [0,0,0,0,0,0,0,0]
OC = [32,32,32,128,240,240,192,128]
EX = [96,96,96,192,192,192,144,240]
WB = [0,0,0,0,0,0,0,0]
DataDepen = [32,32,32,112,112,112,72,144]
ibuffer = [0,0,0,0,0,0,0,0]
iss2iss = [64,64,64,80,80,80,72,112]
MEM = [0,0,0,0,0,0,0,0]
Fetch_wait = [0,0,0,0,0,0,0,0]
Issue_wait = [800,800,832,944,944,976,960,872]
OC_wait = [0,0,0,0,0,0,0,0]
EX_wait = [0,0,0,0,0,0,0,0]
WB_wait = [0,0,0,0,0,0,0,0]
barrier = [0,0,0,0,0,0,0,0]
global_access = [0,0,0,0,0,0,0,0]
ALU = [880,912,912,992,1024,1024,968,968]
INTP = [496,496,496,2256,2256,2256,1400,2568]
ret = [3488,3487,3486,2889,2885,2877,2869,2437]
warp_schedule_latency = [2,2,2,1,1,1,1,2]
icnt = [0,0,0,0,0,0,0,800]
L1 = [0,0,0,7344,7232,7232,3560,0]
L2 = [0,0,0,0,0,0,0,9824]
control = [448,448,448,10000,10000,10000,14776,20856]
plt.bar(store30_x,undefined,label="undefined",color='white')
plt.bar(store30_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store30_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store30_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store30_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store30_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store30_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store30_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store30_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store30_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store30_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store30_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store30_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store30_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store30_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store30_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store30_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store30_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store30_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store30_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store30_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store31_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store31_x,undefined,label="undefined",color='white')
plt.bar(store31_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store31_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store31_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store31_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store31_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store31_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store31_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store31_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store31_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store31_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store31_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store31_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store31_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store31_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store31_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store31_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store31_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store31_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store31_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store31_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store32_x = [element + w +length_stack for element in range(4)]
length_stack = length_stack + 4 + w + 1

undefined = [0,0,0,0]
FD = [96,96,96,103]
Iss = [0,0,0,0]
OC = [32,224,256,334]
EX = [96,96,96,383]
WB = [0,0,0,0]
DataDepen = [32,32,32,46]
ibuffer = [0,0,0,0]
iss2iss = [64,64,64,64]
MEM = [0,0,0,0]
Fetch_wait = [0,0,0,0]
Issue_wait = [32,0,0,32]
OC_wait = [0,0,0,0]
EX_wait = [0,0,0,0]
WB_wait = [0,0,0,0]
barrier = [0,0,0,0]
global_access = [0,0,0,0]
ALU = [640,384,384,573]
INTP = [320,288,288,736]
ret = [7124,7123,7122,6784]
warp_schedule_latency = [1,1,1,1]
icnt = [0,0,0,98]
L1 = [0,160,160,96]
L2 = [0,0,0,1281]
control = [32,32,32,8382]
plt.bar(store32_x,undefined,label="undefined",color='white')
plt.bar(store32_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store32_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store32_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store32_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store32_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store32_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store32_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store32_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store32_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store32_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store32_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store32_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store32_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store32_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store32_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store32_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store32_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store32_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store32_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store32_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store33_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store33_x,undefined,label="undefined",color='white')
plt.bar(store33_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store33_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store33_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store33_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store33_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store33_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store33_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store33_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store33_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store33_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store33_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store33_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store33_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store33_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store33_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store33_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store33_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store33_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store33_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store33_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store34_x = [element + w +length_stack for element in range(8)]
length_stack = length_stack + 8 + w + 1

undefined = [0,0,0,0,0,0,0,0]
FD = [128,128,128,128,128,128,128,96]
Iss = [0,0,0,0,0,0,0,0]
OC = [32,32,32,32,116,186,186,74]
EX = [96,96,96,96,180,180,180,222]
WB = [0,0,0,0,0,0,0,0]
DataDepen = [32,32,32,32,102,102,102,130]
ibuffer = [0,0,0,0,0,0,0,0]
iss2iss = [64,64,64,64,78,78,78,110]
MEM = [0,0,0,0,0,0,0,0]
Fetch_wait = [0,0,0,0,0,0,0,0]
Issue_wait = [800,800,832,960,946,946,978,846]
OC_wait = [0,0,0,0,0,0,0,0]
EX_wait = [0,0,0,0,0,0,0,0]
WB_wait = [0,0,0,0,0,0,0,0]
barrier = [0,0,0,0,0,0,0,0]
global_access = [0,0,0,0,0,0,0,0]
ALU = [854,886,886,886,952,984,984,927]
INTP = [474,474,474,488,2014,2014,2014,2287]
ret = [3488,3487,3486,3482,2919,2912,2905,2658]
warp_schedule_latency = [2,2,2,1,1,1,1,2]
icnt = [0,0,0,0,0,0,0,406]
L1 = [0,0,0,0,5992,5922,5922,154]
L2 = [0,0,0,0,0,0,0,6993]
control = [496,496,496,514,10676,10676,10676,17291]
plt.bar(store34_x,undefined,label="undefined",color='white')
plt.bar(store34_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store34_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store34_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store34_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store34_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store34_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store34_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store34_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store34_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store34_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store34_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store34_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store34_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store34_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store34_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store34_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store34_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store34_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store34_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store34_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store35_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store35_x,undefined,label="undefined",color='white')
plt.bar(store35_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store35_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store35_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store35_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store35_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store35_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store35_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store35_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store35_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store35_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store35_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store35_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store35_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store35_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store35_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store35_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store35_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store35_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store35_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store35_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store36_x = [element + w +length_stack for element in range(4)]
length_stack = length_stack + 4 + w + 1

undefined = [0,0,0,0]
FD = [96,96,96,102]
Iss = [0,0,0,0]
OC = [32,224,256,332]
EX = [96,96,96,342]
WB = [0,0,0,0]
DataDepen = [32,32,32,44]
ibuffer = [0,0,0,0]
iss2iss = [64,64,64,64]
MEM = [0,0,0,0]
Fetch_wait = [0,0,0,0]
Issue_wait = [32,0,0,32]
OC_wait = [0,0,0,0]
EX_wait = [0,0,0,0]
WB_wait = [0,0,0,0]
barrier = [0,0,0,0]
global_access = [0,0,0,0]
ALU = [640,384,384,546]
INTP = [320,288,288,672]
ret = [7124,7123,7122,6786]
warp_schedule_latency = [1,1,1,1]
icnt = [0,0,0,72]
L1 = [0,160,160,96]
L2 = [0,0,0,1104]
control = [32,32,32,8690]
plt.bar(store36_x,undefined,label="undefined",color='white')
plt.bar(store36_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store36_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store36_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store36_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store36_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store36_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store36_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store36_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store36_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store36_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store36_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store36_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store36_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store36_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store36_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store36_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store36_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store36_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store36_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store36_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store37_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store37_x,undefined,label="undefined",color='white')
plt.bar(store37_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store37_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store37_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store37_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store37_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store37_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store37_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store37_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store37_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store37_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store37_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store37_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store37_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store37_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store37_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store37_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store37_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store37_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store37_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store37_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store38_x = [element + w +length_stack for element in range(8)]
length_stack = length_stack + 8 + w + 1

undefined = [0,0,0,0,0,0,0,0]
FD = [128,128,128,128,128,128,128,96]
Iss = [0,0,0,0,0,0,0,0]
OC = [32,32,32,32,104,152,92,68]
EX = [96,96,96,96,168,168,132,204]
WB = [0,0,0,0,0,0,0,0]
DataDepen = [32,32,32,32,92,92,62,116]
ibuffer = [0,0,0,0,0,0,0,0]
iss2iss = [64,64,64,64,76,76,70,108]
MEM = [0,0,0,0,0,0,0,0]
Fetch_wait = [0,0,0,0,0,0,0,0]
Issue_wait = [800,800,832,960,948,948,980,844]
OC_wait = [0,0,0,0,0,0,0,0]
EX_wait = [0,0,0,0,0,0,0,0]
WB_wait = [0,0,0,0,0,0,0,0]
barrier = [0,0,0,0,0,0,0,0]
global_access = [0,0,0,0,0,0,0,0]
ALU = [828,860,860,860,912,944,902,886]
INTP = [452,452,452,464,1772,1772,1112,2006]
ret = [3488,3487,3486,3482,2928,2923,2917,2666]
warp_schedule_latency = [2,2,2,1,1,1,1,2]
icnt = [0,0,0,0,0,0,0,300]
L1 = [0,0,0,0,5040,4992,2496,132]
L2 = [0,0,0,0,0,0,0,5946]
control = [544,544,544,564,11688,11688,15018,18574]
plt.bar(store38_x,undefined,label="undefined",color='white')
plt.bar(store38_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store38_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store38_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store38_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store38_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store38_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store38_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store38_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store38_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store38_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store38_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store38_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store38_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store38_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store38_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store38_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store38_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store38_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store38_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store38_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store39_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store39_x,undefined,label="undefined",color='white')
plt.bar(store39_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store39_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store39_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store39_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store39_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store39_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store39_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store39_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store39_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store39_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store39_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store39_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store39_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store39_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store39_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store39_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store39_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store39_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store39_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store39_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store40_x = [element + w +length_stack for element in range(4)]
length_stack = length_stack + 4 + w + 1

undefined = [0,0,0,0]
FD = [96,96,96,101]
Iss = [0,0,0,0]
OC = [32,224,256,330]
EX = [96,96,96,301]
WB = [0,0,0,0]
DataDepen = [32,32,32,42]
ibuffer = [0,0,0,0]
iss2iss = [64,64,64,64]
MEM = [0,0,0,0]
Fetch_wait = [0,0,0,0]
Issue_wait = [32,0,0,32]
OC_wait = [0,0,0,0]
EX_wait = [0,0,0,0]
WB_wait = [0,0,0,0]
barrier = [0,0,0,0]
global_access = [0,0,0,0]
ALU = [640,384,384,519]
INTP = [320,288,288,608]
ret = [7124,7123,7122,6788]
warp_schedule_latency = [1,1,1,1]
icnt = [0,0,0,50]
L1 = [0,160,160,96]
L2 = [0,0,0,925]
control = [32,32,32,8996]
plt.bar(store40_x,undefined,label="undefined",color='white')
plt.bar(store40_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store40_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store40_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store40_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store40_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store40_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store40_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store40_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store40_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store40_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store40_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store40_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store40_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store40_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store40_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store40_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store40_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store40_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store40_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store40_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store41_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store41_x,undefined,label="undefined",color='white')
plt.bar(store41_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store41_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store41_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store41_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store41_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store41_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store41_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store41_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store41_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store41_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store41_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store41_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store41_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store41_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store41_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store41_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store41_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store41_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store41_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store41_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store42_x = [element + w +length_stack for element in range(8)]
length_stack = length_stack + 8 + w + 1

undefined = [0,0,0,0,0,0,0,0]
FD = [128,128,128,128,128,128,128,96]
Iss = [0,0,0,0,0,0,0,0]
OC = [32,32,32,32,32,92,92,62]
EX = [96,96,96,96,96,156,156,186]
WB = [0,0,0,0,0,0,0,0]
DataDepen = [32,32,32,32,32,82,82,102]
ibuffer = [0,0,0,0,0,0,0,0]
iss2iss = [64,64,64,64,64,74,74,106]
MEM = [0,0,0,0,0,0,0,0]
Fetch_wait = [0,0,0,0,0,0,0,0]
Issue_wait = [800,800,832,982,960,950,950,842]
OC_wait = [0,0,0,0,0,0,0,0]
EX_wait = [0,0,0,0,0,0,0,0]
WB_wait = [0,0,0,0,0,0,0,0]
barrier = [0,0,0,0,0,0,0,0]
global_access = [0,0,0,0,0,0,0,0]
ALU = [802,834,834,834,834,872,904,845]
INTP = [430,430,430,430,440,1530,1530,1725]
ret = [3488,3487,3486,3482,3482,2945,2943,2683]
warp_schedule_latency = [2,2,2,1,1,1,1,2]
icnt = [0,0,0,0,0,0,0,210]
L1 = [0,0,0,0,0,4030,4030,110]
L2 = [0,0,0,0,0,0,0,4825]
control = [592,592,592,602,614,12470,12470,19627]
plt.bar(store42_x,undefined,label="undefined",color='white')
plt.bar(store42_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store42_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store42_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store42_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store42_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store42_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store42_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store42_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store42_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store42_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store42_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store42_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store42_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store42_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store42_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store42_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store42_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store42_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store42_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store42_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store43_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store43_x,undefined,label="undefined",color='white')
plt.bar(store43_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store43_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store43_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store43_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store43_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store43_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store43_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store43_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store43_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store43_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store43_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store43_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store43_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store43_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store43_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store43_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store43_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store43_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store43_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store43_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store44_x = [element + w +length_stack for element in range(4)]
length_stack = length_stack + 4 + w + 1

undefined = [0,0,0,0]
FD = [96,96,96,100]
Iss = [0,0,0,0]
OC = [32,224,256,328]
EX = [96,96,96,260]
WB = [0,0,0,0]
DataDepen = [32,32,32,40]
ibuffer = [0,0,0,0]
iss2iss = [64,64,64,64]
MEM = [0,0,0,0]
Fetch_wait = [0,0,0,0]
Issue_wait = [32,0,0,32]
OC_wait = [0,0,0,0]
EX_wait = [0,0,0,0]
WB_wait = [0,0,0,0]
barrier = [0,0,0,0]
global_access = [0,0,0,0]
ALU = [640,384,384,492]
INTP = [320,288,288,544]
ret = [7124,7123,7122,6790]
warp_schedule_latency = [1,1,1,1]
icnt = [0,0,0,32]
L1 = [0,160,160,96]
L2 = [0,0,0,744]
control = [32,32,32,9300]
plt.bar(store44_x,undefined,label="undefined",color='white')
plt.bar(store44_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store44_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store44_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store44_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store44_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store44_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store44_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store44_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store44_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store44_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store44_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store44_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store44_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store44_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store44_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store44_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store44_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store44_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store44_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store44_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store45_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store45_x,undefined,label="undefined",color='white')
plt.bar(store45_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store45_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store45_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store45_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store45_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store45_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store45_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store45_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store45_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store45_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store45_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store45_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store45_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store45_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store45_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store45_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store45_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store45_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store45_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store45_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store46_x = [element + w +length_stack for element in range(8)]
length_stack = length_stack + 8 + w + 1

undefined = [0,0,0,0,0,0,0,0]
FD = [128,128,128,128,128,128,128,96]
Iss = [0,0,0,0,0,0,0,0]
OC = [32,32,32,32,32,56,80,56]
EX = [96,96,96,96,96,120,144,168]
WB = [0,0,0,0,0,0,0,0]
DataDepen = [32,32,32,32,32,52,72,88]
ibuffer = [0,0,0,0,0,0,0,0]
iss2iss = [64,64,64,64,64,68,72,104]
MEM = [0,0,0,0,0,0,0,0]
Fetch_wait = [0,0,0,0,0,0,0,0]
Issue_wait = [800,800,832,984,960,952,952,840]
OC_wait = [0,0,0,0,0,0,0,0]
EX_wait = [0,0,0,0,0,0,0,0]
WB_wait = [0,0,0,0,0,0,0,0]
barrier = [0,0,0,0,0,0,0,0]
global_access = [0,0,0,0,0,0,0,0]
ALU = [776,808,808,808,808,804,864,804]
INTP = [408,408,408,408,416,848,1288,1444]
ret = [3488,3487,3486,3482,3482,2951,2950,2689]
warp_schedule_latency = [2,2,2,1,1,1,1,2]
icnt = [0,0,0,0,0,0,0,136]
L1 = [0,0,0,0,0,1588,3176,88]
L2 = [0,0,0,0,0,0,0,3844]
control = [640,640,640,648,664,15576,13448,20876]
plt.bar(store46_x,undefined,label="undefined",color='white')
plt.bar(store46_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store46_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store46_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store46_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store46_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store46_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store46_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store46_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store46_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store46_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store46_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store46_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store46_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store46_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store46_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store46_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store46_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store46_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store46_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store46_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store47_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store47_x,undefined,label="undefined",color='white')
plt.bar(store47_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store47_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store47_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store47_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store47_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store47_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store47_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store47_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store47_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store47_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store47_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store47_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store47_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store47_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store47_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store47_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store47_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store47_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store47_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store47_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store48_x = [element + w +length_stack for element in range(4)]
length_stack = length_stack + 4 + w + 1

undefined = [0,0,0,0]
FD = [96,96,96,99]
Iss = [0,0,0,0]
OC = [32,224,256,326]
EX = [96,96,96,219]
WB = [0,0,0,0]
DataDepen = [32,32,32,38]
ibuffer = [0,0,0,0]
iss2iss = [64,64,64,64]
MEM = [0,0,0,0]
Fetch_wait = [0,0,0,0]
Issue_wait = [32,0,0,32]
OC_wait = [0,0,0,0]
EX_wait = [0,0,0,0]
WB_wait = [0,0,0,0]
barrier = [0,0,0,0]
global_access = [0,0,0,0]
ALU = [640,384,384,465]
INTP = [320,288,288,480]
ret = [7124,7123,7122,6791]
warp_schedule_latency = [1,1,1,1]
icnt = [0,0,0,18]
L1 = [0,160,160,96]
L2 = [0,0,0,561]
control = [32,32,32,9602]
plt.bar(store48_x,undefined,label="undefined",color='white')
plt.bar(store48_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store48_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store48_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store48_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store48_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store48_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store48_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store48_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store48_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store48_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store48_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store48_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store48_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store48_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store48_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store48_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store48_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store48_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store48_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store48_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store49_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store49_x,undefined,label="undefined",color='white')
plt.bar(store49_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store49_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store49_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store49_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store49_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store49_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store49_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store49_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store49_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store49_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store49_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store49_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store49_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store49_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store49_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store49_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store49_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store49_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store49_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store49_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store50_x = [element + w +length_stack for element in range(8)]
length_stack = length_stack + 8 + w + 1

undefined = [0,0,0,0,0,0,0,0]
FD = [128,128,128,128,128,128,128,96]
Iss = [0,0,0,0,0,0,0,0]
OC = [32,32,32,32,32,32,68,50]
EX = [96,96,96,96,96,96,132,150]
WB = [0,0,0,0,0,0,0,0]
DataDepen = [32,32,32,32,32,32,62,74]
ibuffer = [0,0,0,0,0,0,0,0]
iss2iss = [64,64,64,64,64,64,70,102]
MEM = [0,0,0,0,0,0,0,0]
Fetch_wait = [0,0,0,0,0,0,0,0]
Issue_wait = [800,800,832,954,986,960,954,838]
OC_wait = [0,0,0,0,0,0,0,0]
EX_wait = [0,0,0,0,0,0,0,0]
WB_wait = [0,0,0,0,0,0,0,0]
barrier = [0,0,0,0,0,0,0,0]
global_access = [0,0,0,0,0,0,0,0]
ALU = [750,782,782,750,782,782,824,763]
INTP = [386,386,386,386,386,392,1046,1163]
ret = [3488,3487,3486,3484,3482,3482,2962,2700]
warp_schedule_latency = [2,2,2,1,1,1,1,2]
icnt = [0,0,0,0,0,0,0,78]
L1 = [0,0,0,0,0,0,2310,66]
L2 = [0,0,0,0,0,0,0,2841]
control = [688,688,688,694,694,714,14246,21971]
plt.bar(store50_x,undefined,label="undefined",color='white')
plt.bar(store50_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store50_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store50_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store50_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store50_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store50_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store50_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store50_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store50_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store50_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store50_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store50_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store50_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store50_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store50_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store50_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store50_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store50_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store50_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store50_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store51_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store51_x,undefined,label="undefined",color='white')
plt.bar(store51_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store51_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store51_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store51_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store51_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store51_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store51_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store51_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store51_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store51_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store51_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store51_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store51_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store51_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store51_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store51_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store51_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store51_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store51_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store51_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store52_x = [element + w +length_stack for element in range(4)]
length_stack = length_stack + 4 + w + 1

undefined = [0,0,0,0]
FD = [96,96,96,98]
Iss = [0,0,0,0]
OC = [32,224,256,324]
EX = [96,96,96,178]
WB = [0,0,0,0]
DataDepen = [32,32,32,36]
ibuffer = [0,0,0,0]
iss2iss = [64,64,64,64]
MEM = [0,0,0,0]
Fetch_wait = [0,0,0,0]
Issue_wait = [32,0,0,32]
OC_wait = [0,0,0,0]
EX_wait = [0,0,0,0]
WB_wait = [0,0,0,0]
barrier = [0,0,0,0]
global_access = [0,0,0,0]
ALU = [640,384,384,438]
INTP = [320,288,288,416]
ret = [7124,7123,7122,6792]
warp_schedule_latency = [1,1,1,1]
icnt = [0,0,0,8]
L1 = [0,160,160,96]
L2 = [0,0,0,376]
control = [32,32,32,9902]
plt.bar(store52_x,undefined,label="undefined",color='white')
plt.bar(store52_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store52_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store52_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store52_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store52_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store52_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store52_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store52_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store52_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store52_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store52_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store52_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store52_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store52_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store52_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store52_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store52_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store52_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store52_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store52_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store53_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store53_x,undefined,label="undefined",color='white')
plt.bar(store53_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store53_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store53_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store53_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store53_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store53_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store53_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store53_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store53_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store53_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store53_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store53_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store53_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store53_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store53_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store53_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store53_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store53_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store53_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store53_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store54_x = [element + w +length_stack for element in range(8)]
length_stack = length_stack + 8 + w + 1

undefined = [0,0,0,0,0,0,0,0]
FD = [128,128,128,128,128,128,128,96]
Iss = [0,0,0,0,0,0,0,0]
OC = [32,32,32,32,32,32,40,44]
EX = [96,96,96,96,96,96,108,132]
WB = [0,0,0,0,0,0,0,0]
DataDepen = [32,32,32,32,32,32,42,60]
ibuffer = [0,0,0,0,0,0,0,0]
iss2iss = [64,64,64,64,64,64,66,100]
MEM = [0,0,0,0,0,0,0,0]
Fetch_wait = [0,0,0,0,0,0,0,0]
Issue_wait = [800,800,832,956,988,960,956,836]
OC_wait = [0,0,0,0,0,0,0,0]
EX_wait = [0,0,0,0,0,0,0,0]
WB_wait = [0,0,0,0,0,0,0,0]
barrier = [0,0,0,0,0,0,0,0]
global_access = [0,0,0,0,0,0,0,0]
ALU = [724,756,756,724,756,756,770,722]
INTP = [364,364,364,364,364,368,584,882]
ret = [3488,3487,3486,3484,3482,3482,2966,2704]
warp_schedule_latency = [2,2,2,1,1,1,1,2]
icnt = [0,0,0,0,0,0,0,36]
L1 = [0,0,0,0,0,0,766,44]
L2 = [0,0,0,0,0,0,0,1894]
control = [736,736,736,740,740,764,16252,23218]
plt.bar(store54_x,undefined,label="undefined",color='white')
plt.bar(store54_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store54_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store54_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store54_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store54_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store54_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store54_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store54_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store54_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store54_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store54_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store54_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store54_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store54_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store54_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store54_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store54_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store54_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store54_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store54_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store55_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store55_x,undefined,label="undefined",color='white')
plt.bar(store55_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store55_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store55_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store55_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store55_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store55_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store55_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store55_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store55_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store55_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store55_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store55_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store55_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store55_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store55_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store55_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store55_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store55_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store55_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store55_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store56_x = [element + w +length_stack for element in range(4)]
length_stack = length_stack + 4 + w + 1

undefined = [0,0,0,0]
FD = [96,96,96,97]
Iss = [0,0,0,0]
OC = [32,224,256,322]
EX = [96,96,96,137]
WB = [0,0,0,0]
DataDepen = [32,32,32,34]
ibuffer = [0,0,0,0]
iss2iss = [64,64,64,64]
MEM = [0,0,0,0]
Fetch_wait = [0,0,0,0]
Issue_wait = [32,0,0,32]
OC_wait = [0,0,0,0]
EX_wait = [0,0,0,0]
WB_wait = [0,0,0,0]
barrier = [0,0,0,0]
global_access = [0,0,0,0]
ALU = [640,384,384,411]
INTP = [320,288,288,352]
ret = [7124,7123,7122,6793]
warp_schedule_latency = [1,1,1,1]
icnt = [0,0,0,2]
L1 = [0,160,160,96]
L2 = [0,0,0,189]
control = [32,32,32,10200]
plt.bar(store56_x,undefined,label="undefined",color='white')
plt.bar(store56_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store56_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store56_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store56_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store56_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store56_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store56_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store56_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store56_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store56_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store56_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store56_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store56_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store56_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store56_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store56_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store56_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store56_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store56_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store56_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store57_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store57_x,undefined,label="undefined",color='white')
plt.bar(store57_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store57_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store57_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store57_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store57_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store57_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store57_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store57_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store57_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store57_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store57_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store57_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store57_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store57_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store57_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store57_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store57_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store57_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store57_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store57_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store58_x = [element + w +length_stack for element in range(8)]
length_stack = length_stack + 8 + w + 1

undefined = [0,0,0,0,0,0,0,0]
FD = [128,128,128,128,128,128,128,96]
Iss = [0,0,0,0,0,0,0,0]
OC = [32,32,32,32,32,32,32,38]
EX = [96,96,96,96,96,96,96,114]
WB = [0,0,0,0,0,0,0,0]
DataDepen = [32,32,32,32,32,32,32,46]
ibuffer = [0,0,0,0,0,0,0,0]
iss2iss = [64,64,64,64,64,64,64,98]
MEM = [0,0,0,0,0,0,0,0]
Fetch_wait = [0,0,0,0,0,0,0,0]
Issue_wait = [800,800,832,958,958,990,960,834]
OC_wait = [0,0,0,0,0,0,0,0]
EX_wait = [0,0,0,0,0,0,0,0]
WB_wait = [0,0,0,0,0,0,0,0]
barrier = [0,0,0,0,0,0,0,0]
global_access = [0,0,0,0,0,0,0,0]
ALU = [698,730,730,698,730,730,730,681]
INTP = [342,342,342,342,342,342,344,601]
ret = [3488,3487,3486,3484,3483,3482,3482,2709]
warp_schedule_latency = [2,2,2,1,1,1,1,2]
icnt = [0,0,0,0,0,0,0,10]
L1 = [0,0,0,0,0,0,0,22]
L2 = [0,0,0,0,0,0,0,945]
control = [784,784,784,786,786,786,814,24419]
plt.bar(store58_x,undefined,label="undefined",color='white')
plt.bar(store58_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store58_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store58_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store58_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store58_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store58_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store58_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store58_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store58_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store58_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store58_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store58_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store58_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store58_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store58_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store58_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store58_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store58_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store58_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store58_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


store59_x = [element + w +length_stack for element in range(0)]
length_stack = length_stack + 0 + w + 1

undefined = [0]
FD = [0]
Iss = [0]
OC = [0]
EX = [0]
WB = [0]
DataDepen = [0]
ibuffer = [0]
iss2iss = [0]
MEM = [0]
Fetch_wait = [0]
Issue_wait = [0]
OC_wait = [0]
EX_wait = [0]
WB_wait = [0]
barrier = [0]
global_access = [0]
ALU = [0]
INTP = [0]
ret = [0]
warp_schedule_latency = [0]
icnt = [0]
L1 = [0]
L2 = [0]
control = [0]
plt.bar(store59_x,undefined,label="undefined",color='white')
plt.bar(store59_x,warp_schedule_latency,bottom=np.array(undefined),label="warp_schedule_latency",color='cyan')
plt.bar(store59_x,ret,bottom=np.array(undefined)+np.array(warp_schedule_latency), label="ret",color='black')
plt.bar(store59_x,FD,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret),label="FD",color='brown')
plt.bar(store59_x,OC,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss),label="OC",color='green')
plt.bar(store59_x,EX,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC),label="EX",color='linen')
plt.bar(store59_x,WB,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX),label="WB",color='purple')
plt.bar(store59_x,DataDepen,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB),label="DataDepen",color='gray')
plt.bar(store59_x,ibuffer,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen),label="ibuffer",color='olive')
plt.bar(store59_x,iss2iss,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer),label="iss2iss",color='cyan')
plt.bar(store59_x,MEM,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss),label="dram_lat",color='yellow')
plt.bar(store59_x,Fetch_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM),label="Fetch_wait",color='black')
plt.bar(store59_x,Issue_wait,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait),label="Issue_wait",color='navy')
plt.bar(store59_x,barrier,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait),label="barrier",color='pink')
plt.bar(store59_x,global_access,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier),label="dram_access",color='orange')
plt.bar(store59_x,ALU,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access),label="ALU",color='purple')
plt.bar(store59_x,INTP,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU),label="INTP",color='greenyellow')
plt.bar(store59_x,icnt,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP),label="icnt",color='red')
plt.bar(store59_x,L1,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt),label="L1",color='royalblue')
plt.bar(store59_x,L2,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1),label="L2",color='lightcoral')
plt.bar(store59_x,control,bottom=np.array(undefined)+np.array(warp_schedule_latency)+np.array(ret)+np.array(FD)+np.array(Iss)+np.array(OC)+np.array(EX)+np.array(WB)+np.array(DataDepen)+np.array(ibuffer)+np.array(iss2iss)+np.array(MEM)+np.array(Fetch_wait)+np.array(Issue_wait)+np.array(barrier)+np.array(global_access)+np.array(ALU)+np.array(INTP)+np.array(icnt)+np.array(L1)+np.array(L2),label="control",color='gray')


#plt.show()
plt.savefig('out.png')
